USE [master]
GO
/****** Object:  Database [Quizapp]    Script Date: 3/17/2021 4:34:28 PM ******/
CREATE DATABASE [Quizapp]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'Quizapp', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.SQLEXPRESS\MSSQL\DATA\Quizapp.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'Quizapp_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.SQLEXPRESS\MSSQL\DATA\Quizapp_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
 WITH CATALOG_COLLATION = DATABASE_DEFAULT
GO
ALTER DATABASE [Quizapp] SET COMPATIBILITY_LEVEL = 140
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [Quizapp].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [Quizapp] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [Quizapp] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [Quizapp] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [Quizapp] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [Quizapp] SET ARITHABORT OFF 
GO
ALTER DATABASE [Quizapp] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [Quizapp] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [Quizapp] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [Quizapp] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [Quizapp] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [Quizapp] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [Quizapp] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [Quizapp] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [Quizapp] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [Quizapp] SET  DISABLE_BROKER 
GO
ALTER DATABASE [Quizapp] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [Quizapp] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [Quizapp] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [Quizapp] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [Quizapp] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [Quizapp] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [Quizapp] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [Quizapp] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [Quizapp] SET  MULTI_USER 
GO
ALTER DATABASE [Quizapp] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [Quizapp] SET DB_CHAINING OFF 
GO
ALTER DATABASE [Quizapp] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [Quizapp] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [Quizapp] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [Quizapp] SET ACCELERATED_DATABASE_RECOVERY = OFF  
GO
ALTER DATABASE [Quizapp] SET QUERY_STORE = OFF
GO
USE [Quizapp]
GO
/****** Object:  Table [dbo].[Answers]    Script Date: 3/17/2021 4:34:28 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Answers](
	[Id] [uniqueidentifier] NOT NULL,
	[OptionId] [uniqueidentifier] NOT NULL,
	[IsCorrect] [bit] NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Options]    Script Date: 3/17/2021 4:34:28 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Options](
	[Id] [uniqueidentifier] NOT NULL,
	[QuestionId] [uniqueidentifier] NOT NULL,
	[Answer] [nvarchar](max) NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PlayState]    Script Date: 3/17/2021 4:34:28 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PlayState](
	[Id] [uniqueidentifier] NOT NULL,
	[UserId] [uniqueidentifier] NOT NULL,
	[QuizzesId] [uniqueidentifier] NOT NULL,
	[IsComplete] [bit] NOT NULL,
	[QuestionId] [uniqueidentifier] NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Questions]    Script Date: 3/17/2021 4:34:28 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Questions](
	[Id] [uniqueidentifier] NOT NULL,
	[QuizzesId] [uniqueidentifier] NOT NULL,
	[Question] [nvarchar](max) NOT NULL,
	[Points] [int] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[QuizStates]    Script Date: 3/17/2021 4:34:28 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[QuizStates](
	[Id] [int] NOT NULL,
	[Description] [nvarchar](15) NOT NULL,
 CONSTRAINT [PK_QuizStates] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Quizzes]    Script Date: 3/17/2021 4:34:28 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Quizzes](
	[Id] [uniqueidentifier] NOT NULL,
	[Title] [varchar](255) NOT NULL,
	[Status] [int] NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Results]    Script Date: 3/17/2021 4:34:28 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Results](
	[Id] [uniqueidentifier] NOT NULL,
	[UserId] [uniqueidentifier] NOT NULL,
	[QuizId] [uniqueidentifier] NOT NULL,
	[QuestionId] [uniqueidentifier] NOT NULL,
	[Score] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Answers] ADD  CONSTRAINT [DF_Answers_Id]  DEFAULT (newid()) FOR [Id]
GO
ALTER TABLE [dbo].[Answers] ADD  CONSTRAINT [DF_QuestionOptions_IsCorrect]  DEFAULT ((0)) FOR [IsCorrect]
GO
ALTER TABLE [dbo].[Options] ADD  CONSTRAINT [DF_Options_Id]  DEFAULT (newid()) FOR [Id]
GO
ALTER TABLE [dbo].[PlayState] ADD  CONSTRAINT [DF_UserConfig_Id]  DEFAULT (newid()) FOR [Id]
GO
ALTER TABLE [dbo].[PlayState] ADD  CONSTRAINT [DF_UserConfig_IsComplete]  DEFAULT ((0)) FOR [IsComplete]
GO
ALTER TABLE [dbo].[Questions] ADD  CONSTRAINT [DF_Questions_Id]  DEFAULT (newid()) FOR [Id]
GO
ALTER TABLE [dbo].[Questions] ADD  CONSTRAINT [DF_Questions_Points]  DEFAULT ((0)) FOR [Points]
GO
ALTER TABLE [dbo].[Quizzes] ADD  CONSTRAINT [DF_Quizzes_Id]  DEFAULT (newid()) FOR [Id]
GO
ALTER TABLE [dbo].[Quizzes] ADD  CONSTRAINT [DF_Quizzes_Status]  DEFAULT ((0)) FOR [Status]
GO
ALTER TABLE [dbo].[Results] ADD  CONSTRAINT [DF_Results_Id]  DEFAULT (newid()) FOR [Id]
GO
ALTER TABLE [dbo].[Results] ADD  CONSTRAINT [DF_UserResults_Score]  DEFAULT ((0)) FOR [Score]
GO
USE [master]
GO
ALTER DATABASE [Quizapp] SET  READ_WRITE 
GO
