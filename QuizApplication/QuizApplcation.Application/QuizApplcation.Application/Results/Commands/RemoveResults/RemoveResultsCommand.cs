﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace QuizApplcation.Application.Results.Commands.RemoveResults
{
    public class RemoveResultsCommand : IRequest<int>
    {        
        public Guid QuizId { get; set; }
    }
}

