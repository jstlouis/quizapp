﻿using MediatR;
using QuizApplcation.Application.Interfaces;
using System;
using System.Threading;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace QuizApplcation.Application.Results.Commands.RemoveResults
{
    public class RemoveResultsCommandHandler : IRequestHandler<RemoveResultsCommand, int>
    {
        public readonly IDatabaseService _database;

        public RemoveResultsCommandHandler(IDatabaseService database)
        {
            _database = database;
        }

        public async Task<int> Handle(RemoveResultsCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var entities = await _database.Results
                                              .Where(p => p.QuizId == request.QuizId)
                                              .AsNoTracking()
                                              .ToListAsync();

                _database.Results.RemoveRange(entities);

                var result = await _database.SaveChangesAsync();

                return result;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}
