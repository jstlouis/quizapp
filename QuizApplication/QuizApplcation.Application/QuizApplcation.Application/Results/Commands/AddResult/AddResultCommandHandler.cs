﻿using MediatR;
using Microsoft.Extensions.DependencyInjection;
using QuizApplcation.Application.Interfaces;
using QuizApplcation.Application.Results.Commands.AddResult.Factory;
using QuizApplcation.Domain.Entities;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace QuizApplcation.Application.Results.Commands.AddResult
{
    public class AddResultCommandHandler : IRequestHandler<AddResultCommand, Tuple<int, ResultEntity>>
    {
        public readonly IDatabaseService _database;
        public readonly IAddResultFactory _factory;
        public readonly IServiceScopeFactory _scopeFactory;

        public AddResultCommandHandler(IDatabaseService database,
                                       IAddResultFactory factory,
                                       IServiceScopeFactory scopeFactory)
        {
            _database = database;
            _factory = factory;
            _scopeFactory = scopeFactory;
        }

        public async Task<Tuple<int, ResultEntity>> Handle(AddResultCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var entity = _factory.Create(request.Model.UserId,
                                             request.Model.QuizId,
                                             request.Model.QuestionId,
                                             request.Model.Score);

                // Source: https://stackoverflow.com/questions/48368634/how-should-i-inject-a-dbcontext-instance-into-an-ihostedservice
                using (var scope = _scopeFactory.CreateScope())
                {
                    var _database = scope.ServiceProvider.GetRequiredService<IDatabaseService>();

                    _database.Results.Add(entity);

                    var result = await _database.SaveChangesAsync();

                    return new Tuple<int, ResultEntity>(result, entity);
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}
