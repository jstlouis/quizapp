﻿using QuizApplcation.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace QuizApplcation.Application.Results.Commands.AddResult.Factory
{
    public class AddResultFactory : IAddResultFactory
    {
        public ResultEntity Create(Guid UserId, Guid QuizId, Guid QuestionId, int Score)
        {
            var entity = new ResultEntity()
            {
                UserId = UserId,
                QuizId = QuizId,
                QuestionId = QuestionId,
                Score = Score
            };

            return entity;
        }
    }
}
