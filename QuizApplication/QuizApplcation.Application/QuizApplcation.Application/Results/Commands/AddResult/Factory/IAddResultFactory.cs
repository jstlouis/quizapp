﻿using QuizApplcation.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace QuizApplcation.Application.Results.Commands.AddResult.Factory
{
    public interface IAddResultFactory
    {
        public ResultEntity Create(Guid UserId,
                                   Guid QuizId,
                                   Guid QuestionId,
                                   int Score);
    }
}
