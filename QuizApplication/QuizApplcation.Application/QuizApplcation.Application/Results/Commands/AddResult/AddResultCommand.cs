﻿using MediatR;
using QuizApplcation.Application.Models;
using QuizApplcation.Domain.Entities;
using System;

namespace QuizApplcation.Application.Results.Commands.AddResult
{
    public class AddResultCommand : IRequest<Tuple<int, ResultEntity>>
    {
        public AddResultModel Model { get; set; }
    }
}
