﻿using QuizApplcation.Application.Common;
using QuizApplcation.Domain.Entities;
using System;
using System.Linq.Expressions;

namespace QuizApplcation.Application.Results.Specifications
{
    public class QuizIdIsSpecification : BaseSpecification<ResultEntity>
    {
        private readonly Guid _quizId;

        public QuizIdIsSpecification(Guid quizId)
        {
            _quizId = quizId;
        }

        public override Expression<Func<ResultEntity, bool>> ToExpression()
        {
            return p => p.QuizId == _quizId;
        }
    }
}
