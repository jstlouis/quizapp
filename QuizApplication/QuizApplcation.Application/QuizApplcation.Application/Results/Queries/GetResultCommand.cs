﻿using MediatR;
using QuizApplcation.Application.Common;
using QuizApplcation.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace QuizApplcation.Application.Results.Queries
{
    public class GetResultCommand : IRequest<List<ResultEntity>>
    {
        public BaseSpecification<ResultEntity> Specifications { get; set; }
    }
}
