﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using QuizApplcation.Application.Interfaces;
using QuizApplcation.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace QuizApplcation.Application.Results.Queries
{
    public class GetResultQueryHandler : IRequestHandler<GetResultCommand, List<ResultEntity>>
    {
        public readonly IDatabaseService _database;

        public GetResultQueryHandler(IDatabaseService database)
        {
            _database = database;
        }

        public async Task<List<ResultEntity>> Handle(GetResultCommand request, CancellationToken cancellationToken)
        {
            var entities = await _database.Results
                                          .Where(request.Specifications.ToExpression())
                                          .AsNoTracking()
                                          .ToListAsync();
            return entities;
        }
    }
}
