﻿using MediatR;
using QuizApplcation.Application.Interfaces;
using QuizApplcation.Application.Quizzes.Commands.GetQuizzes.Factory;
using QuizApplcation.Domain.Entities;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace QuizApplcation.Application.Quizzes.Commands.GetQuizzes
{
    public class AddQuizCommandHandler : IRequestHandler<AddQuizCommand, Tuple<int, QuizEntity>>
    {
        public readonly IDatabaseService _database;
        public readonly IAddQuizFactory _factory;

        public AddQuizCommandHandler(IDatabaseService database,
                                     IAddQuizFactory factory)
        {
            _database = database;
            _factory = factory;
        }

        public async Task<Tuple<int, QuizEntity>> Handle(AddQuizCommand request, CancellationToken cancellationToken)
        {
            try
            {  
                var entity = _factory.Create(request.Model.Title);

                _database.Quizzes.Add(entity);

                var result = await _database.SaveChangesAsync();

                return new Tuple<int, QuizEntity>(result, entity);
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}
