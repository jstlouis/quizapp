﻿using MediatR;
using QuizApplcation.Application.Models;
using QuizApplcation.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace QuizApplcation.Application.Quizzes.Commands.GetQuizzes
{
    public class AddQuizCommand : IRequest<Tuple<int, QuizEntity>>
    {
        public AddQuizModel Model { get; set; }
    }
}
