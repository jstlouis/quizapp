﻿using QuizApplcation.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace QuizApplcation.Application.Quizzes.Commands.GetQuizzes.Factory
{
    public class AddQuizFactory : IAddQuizFactory
    {
        public QuizEntity Create(string title)
        {
            var entity = new QuizEntity()
            {
                Title = title
            };

            return entity;
        }
    }
}
