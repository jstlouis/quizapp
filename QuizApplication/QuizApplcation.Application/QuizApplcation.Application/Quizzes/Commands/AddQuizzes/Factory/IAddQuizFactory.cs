﻿using QuizApplcation.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace QuizApplcation.Application.Quizzes.Commands.GetQuizzes.Factory
{
    public interface IAddQuizFactory
    {
        public QuizEntity Create(string title);
    }
}
