﻿using QuizApplcation.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace QuizApplcation.Application.Quizzes.Commands.UpdateQuiz.Factory
{
    public interface IUpdateQuizFactory
    {
        public QuizEntity Create(Guid Id,
                                 int Status,
                                 String Title);
    }
}
