﻿using QuizApplcation.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace QuizApplcation.Application.Quizzes.Commands.UpdateQuiz.Factory
{
    public class UpdateQuizFactory : IUpdateQuizFactory
    {
        public QuizEntity Create(Guid Id, int Status, string Title)
        {
            var entity = new QuizEntity()
            {
                Id = Id,
                Title = Title,
                Status = Status
            };

            return entity;
        }
    }
}
