﻿using MediatR;
using QuizApplcation.Application.Interfaces;
using QuizApplcation.Application.Quizzes.Commands.UpdateQuiz.Factory;
using QuizApplcation.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace QuizApplcation.Application.Quizzes.Commands.UpdateQuiz
{
    public class UpdateQuizCommandHandler : IRequestHandler<UpdateQuizCommand, Tuple<int, QuizEntity>>
    {
        public readonly IDatabaseService _database;
        public readonly IUpdateQuizFactory _factory;

        public UpdateQuizCommandHandler(IDatabaseService database,
                                     IUpdateQuizFactory factory)
        {
            _database = database;
            _factory = factory;
        }

        public async Task<Tuple<int, QuizEntity>> Handle(UpdateQuizCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var entity = _factory.Create(request.Model.Id,
                                             request.Model.Status,
                                             request.Model.Title);

                _database.AutoDetectChangesEnabled = false;

                _database.Quizzes.Attach(entity);

                _database.SetPropertyModified(entity, "Status", true);

                var result = await _database.SaveChangesAsync();

                _database.AutoDetectChangesEnabled = true;

                return new Tuple<int, QuizEntity>(result, entity);
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}
