﻿using MediatR;
using QuizApplcation.Application.Models;
using QuizApplcation.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace QuizApplcation.Application.Quizzes.Commands.UpdateQuiz
{
    public class UpdateQuizCommand : IRequest<Tuple<int, QuizEntity>>
    {
        public UpdateQuizModel Model { get; set; }
    }
}
