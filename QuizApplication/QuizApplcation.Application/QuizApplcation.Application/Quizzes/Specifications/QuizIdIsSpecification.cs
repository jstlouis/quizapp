﻿using QuizApplcation.Application.Common;
using QuizApplcation.Domain.Entities;
using System;
using System.Linq.Expressions;

namespace QuizApplcation.Application.Quizzes.Specifications
{
    public class QuizIdIsSpecification : BaseSpecification<QuizEntity>
    {
        private readonly Guid _id;

        public QuizIdIsSpecification(Guid id)
        {
            _id = id;
        }

        public override Expression<Func<QuizEntity, bool>> ToExpression()
        {
            return p => p.Id == _id;
        }
    }
}
