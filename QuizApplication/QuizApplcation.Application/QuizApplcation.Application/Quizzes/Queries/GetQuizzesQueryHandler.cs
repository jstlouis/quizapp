﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using QuizApplcation.Application.Interfaces;
using QuizApplcation.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace QuizApplcation.Application.Quizzes.Queries
{
    public class GetQuizzesQueryHandler : IRequestHandler<GetQuizzesCommand, List<QuizEntity>>
    {
        public readonly IDatabaseService _database;

        public GetQuizzesQueryHandler(IDatabaseService database)
        {
            _database = database;
        }

        public async Task<List<QuizEntity>> Handle(GetQuizzesCommand request, CancellationToken cancellationToken)
        {
            var entities = await _database.Quizzes
                                          //.Include(q => q.Questions)
                                          //.ThenInclude(o => o.Options)
                                          //.ThenInclude(a => a.AnswerKey)
                                          .Include(s => s.State)
                                          .Where(request.Specifications.ToExpression())
                                          .AsNoTracking()
                                          .ToListAsync();

            return entities;
        }
    }
}
