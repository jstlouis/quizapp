﻿using MediatR;
using QuizApplcation.Application.Common;
using QuizApplcation.Domain.Entities;
using System.Collections.Generic;

namespace QuizApplcation.Application.Quizzes.Queries
{
    public class GetQuizzesCommand : IRequest<List<QuizEntity>>
    {
        public BaseSpecification<QuizEntity> Specifications { get; set; }
    }
}
