﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using QuizApplcation.Application.Interfaces;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace QuizApplcation.Application.PlayState.Command.RemovePlayState
{
    public class RemovePlayStateCommandHandler : IRequestHandler<RemovePlayStateCommand, int>
    {
        public readonly IDatabaseService _database;

        public RemovePlayStateCommandHandler(IDatabaseService database)
        {
            _database = database;
        }

        public async Task<int> Handle(RemovePlayStateCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var entities = await _database.PlayState
                                              .Where(p => p.QuizId == request.QuizId)
                                              .AsNoTracking()
                                              .ToListAsync();

                _database.PlayState.RemoveRange(entities);

                var result = await _database.SaveChangesAsync();

                return result;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}
