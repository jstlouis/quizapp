﻿using MediatR;
using QuizApplcation.Application.Common;
using QuizApplcation.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace QuizApplcation.Application.PlayState.Command.RemovePlayState
{
    public class RemovePlayStateCommand : IRequest<int>
    {
        public Guid QuizId { get; set; }
    }
}
