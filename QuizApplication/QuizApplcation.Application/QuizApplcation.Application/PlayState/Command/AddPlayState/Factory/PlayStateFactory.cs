﻿using QuizApplcation.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace QuizApplcation.Application.PlayState.Command.AddPlayState.Factory
{
    public class PlayStateFactory : IPlayStateFactory
    {
        public PlayStateEntity Create(Guid UserId, Guid QuizId, Guid QuestionId, bool IsComplete)
        {
            var entity = new PlayStateEntity()
            {
                UserId = UserId,
                QuizId = QuizId,
                QuestionId = QuestionId,
                IsComplete = IsComplete
            };

            return entity;
        }
    }
}
