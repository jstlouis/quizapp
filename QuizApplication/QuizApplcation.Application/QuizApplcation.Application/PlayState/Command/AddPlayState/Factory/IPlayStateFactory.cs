﻿using QuizApplcation.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace QuizApplcation.Application.PlayState.Command.AddPlayState.Factory
{
    public interface IPlayStateFactory
    {
        public PlayStateEntity Create(Guid UserId,
                                      Guid QuizId,
                                      Guid QuestionId,
                                      bool IsComplete);
    }
}
