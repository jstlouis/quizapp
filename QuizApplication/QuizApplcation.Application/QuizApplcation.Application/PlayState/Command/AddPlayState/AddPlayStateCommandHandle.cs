﻿using MediatR;
using QuizApplcation.Application.Interfaces;
using QuizApplcation.Application.PlayState.Command.AddPlayState.Factory;
using QuizApplcation.Domain.Entities;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace QuizApplcation.Application.PlayState.Command.AddPlayState
{
    public class AddPlayStateCommandHandle : IRequestHandler<AddPlayStateCommand, Tuple<int, PlayStateEntity>>
    {
        public readonly IDatabaseService _database;
        public readonly IPlayStateFactory _factory;

        public AddPlayStateCommandHandle(IDatabaseService database,
                                      IPlayStateFactory factory)
        {
            _database = database;
            _factory = factory;
        }

        public async Task<Tuple<int, PlayStateEntity>> Handle(AddPlayStateCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var entity = _factory.Create(request.Model.UserId,
                                             request.Model.QuizzesId,
                                             request.Model.QuestionId,
                                             request.Model.IsComplete);

                _database.PlayState.Add(entity);

                var result = await _database.SaveChangesAsync();

                return new Tuple<int, PlayStateEntity>(result, entity);
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}
