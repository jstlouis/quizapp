﻿using MediatR;
using QuizApplcation.Application.Models;
using QuizApplcation.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace QuizApplcation.Application.PlayState.Command.AddPlayState
{
    public class AddPlayStateCommand : IRequest<Tuple<int, PlayStateEntity>>
    {
        public AddPlayStateModel Model { get; set; }
    }
}
