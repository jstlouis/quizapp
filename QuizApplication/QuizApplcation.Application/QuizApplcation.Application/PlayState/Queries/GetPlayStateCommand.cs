﻿using MediatR;
using QuizApplcation.Application.Common;
using QuizApplcation.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace QuizApplcation.Application.PlayState.Queries
{
    public class GetPlayStateCommand : IRequest<List<PlayStateEntity>>
    {
        public BaseSpecification<PlayStateEntity> Specifications { get; set; }
    }
}
