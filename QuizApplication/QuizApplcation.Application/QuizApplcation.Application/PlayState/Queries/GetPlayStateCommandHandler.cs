﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using QuizApplcation.Application.Interfaces;
using QuizApplcation.Application.PlayState.Command.AddPlayState.Factory;
using QuizApplcation.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace QuizApplcation.Application.PlayState.Queries
{
    public class GetPlayStateCommandHandler : IRequestHandler<GetPlayStateCommand, List<PlayStateEntity>>
    {
        public readonly IDatabaseService _database;

        public GetPlayStateCommandHandler(IDatabaseService database)
        {
            _database = database;
        }

        public async Task<List<PlayStateEntity>> Handle(GetPlayStateCommand request, CancellationToken cancellationToken)
        {
            var entities = await _database.PlayState
                                          .Where(request.Specifications.ToExpression())
                                          .AsNoTracking()
                                          .ToListAsync();

            return entities;
        }
    }
}
