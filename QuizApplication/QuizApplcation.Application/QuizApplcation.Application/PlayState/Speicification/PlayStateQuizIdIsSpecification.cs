﻿using QuizApplcation.Application.Common;
using QuizApplcation.Domain.Entities;
using System;
using System.Linq.Expressions;

namespace QuizApplcation.Application.PlayState.Specifications
{
    public class PlayStateQuizIdIsSpecification : BaseSpecification<PlayStateEntity>
    {
        private readonly Guid _quizId;

        public PlayStateQuizIdIsSpecification(Guid quizId)
        {
            _quizId = quizId;
        }

        public override Expression<Func<PlayStateEntity, bool>> ToExpression()
        {
            return p => p.QuizId == _quizId;
        }
    }
}
