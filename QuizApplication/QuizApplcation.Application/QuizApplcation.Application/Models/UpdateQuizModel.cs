﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace QuizApplcation.Application.Models
{
    public class UpdateQuizModel
    {
        [Required]
        public Guid Id { get; set; }

        [Required]
        public int Status { get; set; }

        [Required]
        [MaxLength(255)]
        public string Title { get; set; }
    }
}
