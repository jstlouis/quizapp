﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace QuizApplcation.Application.Models
{
    public class AddResultModel
    {
        [Required]
        public Guid UserId { get; set; }

        [Required]
        public Guid QuizId { get; set; }

        [Required]
        public Guid QuestionId { get; set; }

        [Required]
        public int Score { get; set; }
    }
}
