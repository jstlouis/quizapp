﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace QuizApplcation.Application.Models
{
    public class AddPlayStateModel
    {
        [Required]
        public Guid UserId { get; set; }

        [Required]
        public Guid QuizzesId { get; set; }

        [Required]
        public Guid QuestionId { get; set; }

        [Required]
        public bool IsComplete { get; set; }
    }
}
