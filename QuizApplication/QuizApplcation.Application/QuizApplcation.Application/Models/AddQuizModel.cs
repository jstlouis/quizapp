﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace QuizApplcation.Application.Models
{
    public class AddQuizModel
    {
        [Required]
        [MaxLength(255)]
        public string Title { get; set; }
    }
}
