﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace QuizApplcation.Application.Models
{
    public class AddQuestionModel
    {
        [Required]
        public Guid QuizId { get; set; }

        [Required]
        public string Question { get; set; }

        [Required]
        public decimal Points { get; set; } = 0.0m;

        [Required]
        public bool IsCorrect { get; set; }
    }
}
