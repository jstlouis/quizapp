﻿using MediatR;
using QuizApplcation.Application.Interfaces;
using QuizApplcation.Application.Questions.Commands.GetQuestions.Factory;
using QuizApplcation.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace QuizApplcation.Application.Questions.Commands.GetQuestions
{
    public class AddQuestionCommandHandler : IRequestHandler<AddQuestionCommand, Tuple<int, QuestionEntity>>
    {
        public readonly IDatabaseService _database;
        public readonly IAddQuestionFactory _factory;

        public AddQuestionCommandHandler(IDatabaseService database,
                                     IAddQuestionFactory factory)
        {
            _database = database;
            _factory = factory;
        }

        public async Task<Tuple<int, QuestionEntity>> Handle(AddQuestionCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var entity = _factory.Create(request.Model.QuizId,
                                             request.Model.Question,
                                             request.Model.Points,
                                             request.Model.IsCorrect);

                _database.Questions.Add(entity);

                var result = await _database.SaveChangesAsync();

                return new Tuple<int, QuestionEntity>(result, entity);
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}
