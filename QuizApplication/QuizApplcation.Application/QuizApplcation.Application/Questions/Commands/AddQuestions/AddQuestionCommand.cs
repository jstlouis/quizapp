﻿using MediatR;
using QuizApplcation.Application.Models;
using QuizApplcation.Domain.Entities;
using System;

namespace QuizApplcation.Application.Questions.Commands.GetQuestions
{
    public class AddQuestionCommand : IRequest<Tuple<int, QuestionEntity>>
    {
        public AddQuestionModel Model { get; set; }
    }
}
