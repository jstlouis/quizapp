﻿using QuizApplcation.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace QuizApplcation.Application.Questions.Commands.GetQuestions.Factory
{
    public interface IAddQuestionFactory
    {
        public QuestionEntity Create(Guid QuizId,
                                     string Question,
                                     decimal Points,
                                     bool IsCorrect);
    }
}
