﻿using QuizApplcation.Application.Common;
using QuizApplcation.Domain.Entities;
using System;
using System.Linq.Expressions;

namespace QuizApplcation.Application.Questions.Specifications
{
    public class QuestionQuizIdIsSpecification : BaseSpecification<QuestionEntity>
    {
        private readonly Guid _quizId;

        public QuestionQuizIdIsSpecification(Guid quizId)
        {
            _quizId = quizId;
        }

        public override Expression<Func<QuestionEntity, bool>> ToExpression()
        {
            return p => p.QuizId == _quizId;
        }
    }
}
