﻿using QuizApplcation.Application.Common;
using QuizApplcation.Domain.Entities;
using System;
using System.Linq.Expressions;

namespace QuizApplcation.Application.Questions.Specifications
{
    public class QuestionIdIsSpecification : BaseSpecification<QuestionEntity>
    {
        private readonly Guid _Id;

        public QuestionIdIsSpecification(Guid Id)
        {
            _Id = Id;
        }

        public override Expression<Func<QuestionEntity, bool>> ToExpression()
        {
            return p => p.Id == _Id;
        }
    }
}
