﻿using MediatR;
using QuizApplcation.Application.Common;
using QuizApplcation.Domain.Entities;
using System.Collections.Generic;

namespace QuizApplcation.Application.Questions.Queries
{
    public class GetQuestionsCommand : IRequest<List<QuestionEntity>>
    {
        public BaseSpecification<QuestionEntity> Specifications { get; set; }
    }
}
