﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using QuizApplcation.Application.Interfaces;
using QuizApplcation.Domain.Entities;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace QuizApplcation.Application.Questions.Queries
{
    public class GetQuestionsQueryHandler : IRequestHandler<GetQuestionsCommand, List<QuestionEntity>>
    {
        public readonly IDatabaseService _database;

        public GetQuestionsQueryHandler(IDatabaseService database)
        {
            _database = database;
        }

        public async Task<List<QuestionEntity>> Handle(GetQuestionsCommand request, CancellationToken cancellationToken)
        {
            var entities = await _database.Questions
                                          .Include(o => o.Options)
                                          .ThenInclude(a => a.AnswerKey)
                                          .Where(request.Specifications.ToExpression())
                                          .AsNoTracking()
                                          .ToListAsync();

            return entities;
        }
    }
}
