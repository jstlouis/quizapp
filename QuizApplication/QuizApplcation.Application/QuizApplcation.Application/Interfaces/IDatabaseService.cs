﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using QuizApplcation.Domain.Entities;
using QuizApplcation.Domain.SharedKernel;
using System.Threading;
using System.Threading.Tasks;

namespace QuizApplcation.Application.Interfaces
{
    public interface IDatabaseService
    {
        public int SaveChanges();
        
        public Task<int> SaveChangesAsync(CancellationToken cancallationToken = default);
        
        public EntityEntry<T> Entry<T>(T entity) where T : BaseEntity;

        public DbSet<T> Set<T>() where T : BaseEntity;

        public void SetPropertyModified<T>(T entity, string property, bool value) where T : BaseEntity;

        public EntityState GetState<T>(T entity) where T : BaseEntity;

        public bool AutoDetectChangesEnabled { get; set; }

        public void DetectChanges();

        public DbSet<QuizEntity> Quizzes { get; set; }

        public DbSet<QuestionEntity> Questions { get; set; }

        public DbSet<ResultEntity> Results { get; set; }

        public DbSet<PlayStateEntity> PlayState { get; set; }
    }
}
