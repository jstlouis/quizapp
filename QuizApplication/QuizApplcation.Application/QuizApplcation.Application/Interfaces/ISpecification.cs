﻿using System;
using System.Linq.Expressions;

namespace QuizApplcation.Application.Interfaces
{
    public interface ISpecification<T>
    {        
        abstract Expression<Func<T, bool>> ToExpression();

        bool IsSatisfiedBy(T entity);       
    }
}
