﻿using System.Linq.Expressions;

namespace QuizApplcation.Application.Common
{
    // Source: https://github.com/vkhorikov/SpecificationPattern/tree/master/SpecificationPattern
    // Ref only: https://enterprisecraftsmanship.com/posts/specification-pattern-c-implementation/
    internal class ParameterReplacer : ExpressionVisitor
    {
        private readonly ParameterExpression _parameter;

        protected override Expression VisitParameter(ParameterExpression node)

            => base.VisitParameter(_parameter);

        internal ParameterReplacer(ParameterExpression parameter)
        {
            _parameter = parameter;
        }
    }
}
