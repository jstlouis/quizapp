﻿using QuizApplcation.Application.Interfaces;
using System;
using System.Linq.Expressions;

namespace QuizApplcation.Application.Common
{
    // Source: https://github.com/vkhorikov/SpecificationPattern/tree/master/SpecificationPattern
    // Also: Plurasight csharp-specification-pattern by Vladimir Khorikov
    // Additional ref: https://enterprisecraftsmanship.com/posts/specification-pattern-c-implementation/
    public abstract class BaseSpecification<T> : ISpecification<T> 
    {
        public static readonly BaseSpecification<T> All = new IdentitySpecification<T>();

        public abstract Expression<Func<T, bool>> ToExpression();

        public bool IsSatisfiedBy(T entity)
        {
            Func<T, bool> predicate = ToExpression().Compile();
            return predicate(entity);
        }

        public BaseSpecification<T> And(BaseSpecification<T> specification)
        {
            if (this == All || specification == All)
                return All;

            return new AndSpecification<T>(this, specification);
        }

        public BaseSpecification<T> Or(BaseSpecification<T> specification)
        {
            if (this == All || specification == All)
                return All;

            return new OrSpecification<T>(this, specification);
        }

        public BaseSpecification<T> Not()
        {
            return new NotSpecification<T>(this);
        }
    }

    internal sealed class IdentitySpecification<T> : BaseSpecification<T>
    {
        public override Expression<Func<T, bool>> ToExpression()
        {
            return x => true;
        }
    }

    internal sealed class AndSpecification<T> : BaseSpecification<T>
    {

        private readonly BaseSpecification<T> _left;
        private readonly BaseSpecification<T> _right;


        public AndSpecification(BaseSpecification<T> left, BaseSpecification<T> right)
        {
            _right = right;
            _left = left;
        }

        public override Expression<Func<T, bool>> ToExpression()
        {

            Expression<Func<T, bool>> leftExpression = _left.ToExpression();
            Expression<Func<T, bool>> rightExpression = _right.ToExpression();

            var paramExpr = Expression.Parameter(typeof(T));

            var exprBody = Expression.AndAlso(leftExpression.Body, rightExpression.Body);

            exprBody = (BinaryExpression)new ParameterReplacer(paramExpr).Visit(exprBody);

            var finalExpr = Expression.Lambda<Func<T, bool>>(exprBody, paramExpr);

            return finalExpr;

        }

    }

    internal sealed class OrSpecification<T> : BaseSpecification<T>
    {

        private readonly BaseSpecification<T> _left;
        private readonly BaseSpecification<T> _right;

        public OrSpecification(BaseSpecification<T> left, BaseSpecification<T> right)
        {
            _right = right;
            _left = left;
        }

        public override Expression<Func<T, bool>> ToExpression()
        {

            var leftExpression = _left.ToExpression();
            var rightExpression = _right.ToExpression();

            var paramExpr = Expression.Parameter(typeof(T));

            var exprBody = Expression.OrElse(leftExpression.Body, rightExpression.Body);

            exprBody = (BinaryExpression)new ParameterReplacer(paramExpr).Visit(exprBody);

            var finalExpr = Expression.Lambda<Func<T, bool>>(exprBody, paramExpr);

            return finalExpr;

        }

    }

    internal sealed class NotSpecification<T> : BaseSpecification<T>
    {
        private readonly BaseSpecification<T> _spec;

        public NotSpecification(BaseSpecification<T> spec)
        {
            _spec = spec;
        }

        public override Expression<Func<T, bool>> ToExpression()
        {

            var specExpression = _spec.ToExpression();           

            var paramExpr = Expression.Parameter(typeof(T));

            var exprBody = Expression.Not(specExpression.Body);

            exprBody = (UnaryExpression)new ParameterReplacer(paramExpr).Visit(exprBody);

            var finalExpr = Expression.Lambda<Func<T, bool>>(exprBody, paramExpr);

            return finalExpr;

        }
        
    }

}
