﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using QuizApplication.Test.Helpers;
using System.Threading.Tasks;
using QuizApplcation.Application.Results.Queries;
using QuizApplcation.Application.Results.Specifications;
using System;
using System.Threading;
using FluentAssertions;

namespace QuizApplication.Test
{
    [TestClass]
    public class ResultGetCommandTest
    {
        [TestMethod]
        public async Task ShoudReturnResultEquivalentResultEntity()
        {            
            var spec = new QuizIdIsSpecification(Guid.Parse("4df81d9c-eae1-4b55-9814-b241dab4ad59"));

            var expected = ResultsCommandHandlerHelper.MockResultEntity();

            var mockHandler = ResultsCommandHandlerHelper.MockGetResultQueryHandler();

            var entity = await mockHandler.Handle(
                new GetResultCommand() { Specifications = spec }, new CancellationToken());

            expected.Should().BeEquivalentTo(entity[0]);           
        }
    }
}
