﻿using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using QuizApplication.Test.Helpers;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace QuizApplication.Test
{
    [TestClass]
    public class ResultViewModelFactoryTest
    {
        [TestMethod]
        public async Task ShoudReturnResultEquivalentTestListAsync()
        {
            var mockFactory = ResultsViewModelFactoryHelper.MockResultsViewModelFactory();

            var model = await mockFactory.GetScore(Guid.Parse("2124f0aa-39ce-4d47-bc7c-c461503dd7e2"));

            var expected = ResultsViewModelFactoryHelper.MockScoreModel();

            expected.Should().BeEquivalentTo(model);
        }

    }
}
