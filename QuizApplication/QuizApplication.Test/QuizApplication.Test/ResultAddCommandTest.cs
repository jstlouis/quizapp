﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using QuizApplication.Test.Helpers;
using System.Threading.Tasks;
using QuizApplcation.Application.Results.Queries;
using QuizApplcation.Application.Results.Specifications;
using System;
using System.Threading;
using FluentAssertions;
using QuizApplcation.Application.Models;
using QuizApplcation.Application.Results.Commands.AddResult;

namespace QuizApplication.Test
{
    [TestClass]
    public class ResultAddCommandTest
    {        
        [TestMethod]
        public async Task ShoudAddModelAndReturnEntity()
        {
            // mock the command hander
            var mockHandler = ResultsCommandHandlerHelper.MockAddResultCommandHandler();

            // create a model to pass to mock handler
            var model = new AddResultModel()
            {
                UserId = Guid.Parse("14eabb21-176e-45ab-a570-a0a73bdabb89"),
                QuizId = Guid.Parse("4df81d9c-eae1-4b55-9814-b241dab4ad59"),
                QuestionId = Guid.Parse("dc10e716-ac4b-4442-a7d3-c2f78da1bb17"),
                Score = 100
            };

            // get the enities
            var expexted = ResultsCommandHandlerHelper.MockResultEntity();
            
            // call the handler with the model
            var entity = await mockHandler.Handle(
                new AddResultCommand() { Model = model }, new CancellationToken());

            // make sure they match
            Assert.AreEqual(1, entity.Item1);
            Assert.AreEqual(model.UserId, entity.Item2.UserId);
            Assert.AreEqual(model.QuizId, entity.Item2.QuizId);
            Assert.AreEqual(model.QuestionId, entity.Item2.QuestionId);
            Assert.AreEqual(model.Score, entity.Item2.Score);
        }
    }
}
