using Microsoft.VisualStudio.TestTools.UnitTesting;
using QuizApplication.Test.Helpers;
using FluentAssertions;
using System.Threading.Tasks;

namespace QuizApplication.Test
{
    [TestClass]
    public class QuizViewModelFactoryTest

    {
        [TestMethod]
        public async Task ShoudReturnQuizEquivalentTestListAsync()
        {
            // mock the factory
            var mockFactory = QuizModelFactoryTestHelper.MockQuizViewModelFactory();

            // get the mocked list models from the mocked test entities.
            var model = await mockFactory.GetQuizzes();

            // get the expected quiz model list
            var expected = QuizModelFactoryTestHelper.MockQuizModel();

            // models should match
            expected.Should().BeEquivalentTo(model);
        }
    }
}
