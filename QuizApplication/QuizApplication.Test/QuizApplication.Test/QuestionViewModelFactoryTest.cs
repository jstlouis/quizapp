﻿using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using QuizApplication.Test.Helpers;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace QuizApplication.Test
{
    [TestClass]
    public class QuestionViewModelFactoryTest
    {
        [TestMethod]
        public async Task ShoudReturnQuestionEquivalentTestListAsync()
        {
            // get the mocked factory
            var mockFactory = QuestionViewModelFactoryHelper.MockQuestionViewModelFactory();

            // get the mocked question from the mocked question entity.
            var model = await mockFactory.GetQuestions(Guid.Parse("2124f0aa-39ce-4d47-bc7c-c461503dd7e2"));

            // get the model that sounds be returned based on the mocked entity
            var expected = QuestionViewModelFactoryHelper.MockQuestionModel();

            // make sure they are the same
            expected.Should().BeEquivalentTo(model);
        }

    }
}
