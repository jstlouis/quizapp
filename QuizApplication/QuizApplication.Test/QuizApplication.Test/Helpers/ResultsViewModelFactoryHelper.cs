﻿using MediatR;
using Moq;
using Moq.AutoMock;
using QuizApplcation.Application.Questions.Queries;
using QuizApplcation.Application.Results.Queries;
using QuizApplcation.Domain.Entities;
using QuizApplication.RazorPages.Results.Factories;
using QuizApplication.RazorPages.Results.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace QuizApplication.Test.Helpers
{
    public class ResultsViewModelFactoryHelper
    {
        public static IResultsViewModelFactory MockResultsViewModelFactory()
        {
            AutoMocker mocker = new AutoMocker();

            // test entity
            var resultEntities = new List<ResultEntity>()
            {
                new ResultEntity(Guid.Parse("3e1422c2-3b72-44b0-aa72-e20d2dd020ef"))
                {
                    UserId = Guid.Parse("14eabb21-176e-45ab-a570-a0a73bdabb89"),
                    QuizId = Guid.Parse("4df81d9c-eae1-4b55-9814-b241dab4ad59"),
                    QuestionId = Guid.Parse("dc10e716-ac4b-4442-a7d3-c2f78da1bb17"),
                    Score = 100
                }
            };

            // mock mediator, return test entity
            mocker.GetMock<IMediator>()
                  .Setup(p => p.Send(It.IsAny<GetResultCommand>(), It.IsAny<CancellationToken>()))
                  .ReturnsAsync(resultEntities);

            // mock mediator to return the question points.
            // used to get the maxPoints (200).
            mocker.GetMock<IMediator>()
                  .Setup(p => p.Send(It.IsAny<GetQuestionsCommand>(), It.IsAny<CancellationToken>()))
                  .ReturnsAsync(new List<QuestionEntity>()
                      {
                            new QuestionEntity(Guid.Parse("53b0bc98-e11d-454e-b5a1-36b91554fc22"))
                            {
                                Points = 200
                            }
                      });

            // mock the factory
            var mockResultsViewModelFactoryHelper = mocker.CreateInstance<ResultsViewModelFactory>();

            return mockResultsViewModelFactoryHelper;
        }

        public static Score MockScoreModel()
        {
            return new Score()
            {
                score = 100,
                maxScore = 200
            };
        }
    }
}
