﻿using MediatR;
using Microsoft.Extensions.DependencyInjection;
using Moq;
using Moq.AutoMock;
using QuizApplcation.Application.Interfaces;
using QuizApplcation.Application.Questions.Queries;
using QuizApplcation.Application.Results.Commands.AddResult;
using QuizApplcation.Application.Results.Commands.AddResult.Factory;
using QuizApplcation.Domain.Entities;
using QuizApplcation.Infrastructure.Contexts;
using QuizApplication.RazorPages.Questions.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace QuizApplication.Test.Helpers
{
    public static class QuestionServiceHelper
    {
        public static IQuestionService GetMockQuestionService()
        {
            AutoMocker mocker = new AutoMocker();

            // get are test question as a list to return during the test
            var questionEntity = GetMockQuestionEntityList().Where(p => p.Id == Guid.Parse("3600a143-ca5b-4bd4-96af-982f221d64ed")).ToList();

            // mock mediator for getting the question, return test entity
            mocker.GetMock<IMediator>()
                  .Setup(p => p.Send(It.IsAny<GetQuestionsCommand>(), It.IsAny<CancellationToken>()))
                  .ReturnsAsync(questionEntity);
             
            // mock the query handler
            var mockQuestionService = mocker.CreateInstance<QuestionService>();

            return mockQuestionService;
        }

        public static List<QuestionEntity> GetMockQuestionEntityList()
        {
            var questionEntity = new List<QuestionEntity>()
            {
                new QuestionEntity(Guid.Parse("3600a143-ca5b-4bd4-96af-982f221d64ed"))
                {
                    QuizId = Guid.Parse("19a06750-5441-443c-a295-afd16b6a49cf"),
                    Question = "Question test 2",
                    Points = 20,
                    Options = new List<OptionsEntity>()
                    {
                        new OptionsEntity(Guid.Parse("7b26b902-e365-43cb-95ff-ae4a955ba13f"))
                        {
                            QuestionId = Guid.Parse("3600a143-ca5b-4bd4-96af-982f221d64ed"),
                            Answer = "First Answer to Question test 2",
                            AnswerKey = new AnswerEntity(Guid.Parse("5a8c9a22-8158-4a97-a14c-fa53dec68a9e"))
                            {
                                IsCorrect = false,
                                OptionId = Guid.Parse("7b26b902-e365-43cb-95ff-ae4a955ba13f")
                            }
                        },
                        new OptionsEntity(Guid.Parse("f26f5ada-6e10-4025-bee3-44a6f942172b"))
                        {
                            QuestionId = Guid.Parse("3600a143-ca5b-4bd4-96af-982f221d64ed"),
                            Answer = "Second Answer to Question test 2",
                            AnswerKey = new AnswerEntity(Guid.Parse("17516061-9521-4a8c-b0f6-9ade1844f67e"))
                            {
                                IsCorrect = true,
                                OptionId = Guid.Parse("f26f5ada-6e10-4025-bee3-44a6f942172b")
                            }
                        }
                    }
                }
            };

            return questionEntity;
        }
    }
}
