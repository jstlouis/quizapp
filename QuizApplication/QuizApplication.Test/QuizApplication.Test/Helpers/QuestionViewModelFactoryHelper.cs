﻿using AutoMapper;
using MediatR;
using Moq;
using Moq.AutoMock;
using QuizApplcation.Application.PlayState.Queries;
using QuizApplcation.Application.Questions.Queries;
using QuizApplcation.Domain.Entities;
using QuizApplication.RazorPages.Profiles;
using QuizApplication.RazorPages.Questions.Factories;
using QuizApplication.RazorPages.Questions.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace QuizApplication.Test.Helpers
{
    public static class QuestionViewModelFactoryHelper
    {
        public static IQuestionViewModelFactory MockQuestionViewModelFactory()
        {
            AutoMocker mocker = new AutoMocker();

            // mock IMapper
            var quizProfile = new QuestionProfile();
            var configuration = new MapperConfiguration(cfg => cfg.AddProfile(quizProfile));
            var mapper = new Mapper(configuration);
            mocker.Use<IMapper>(mapper);

            // mock a question entity for testing
            var questionEntity = new List<QuestionEntity>()
            {
                new QuestionEntity(Guid.Parse("8ef5219d-c85e-4ba0-b60f-174e5dfc4a53"))
                {                    
                    QuizId = Guid.Parse("19a06750-5441-443c-a295-afd16b6a49cf"),
                    Question = "Question test 1",
                    Points = 10,
                    Options = new List<OptionsEntity>()
                    {
                        new OptionsEntity(Guid.Parse("f4d241f2-052f-4849-8d01-6afb29401e21"))
                        {
                            QuestionId = Guid.Parse("8ef5219d-c85e-4ba0-b60f-174e5dfc4a53"),
                            Answer = "Answer to Question test 1"
                        }
                    }
                }
            };

            // mock mediator to return the above test question entity.
            mocker.GetMock<IMediator>()
                  .Setup(p => p.Send(It.IsAny<GetQuestionsCommand>(), It.IsAny<CancellationToken>()))
                  .ReturnsAsync(questionEntity);

            // mock playstate, just return empty playstate list.
            mocker.GetMock<IMediator>()
                  .Setup(p => p.Send(It.IsAny<GetPlayStateCommand>(), It.IsAny<CancellationToken>()))
                  .ReturnsAsync(new List<PlayStateEntity>());

            // mock the factory
            var mockQuestionViewModelFactory = mocker.CreateInstance<QuestionViewModelFactory>();

            return mockQuestionViewModelFactory;
        }

        public static List<Question> MockQuestionModel()
        {
            var questionList = new List<Question>()
            {
                new Question()
                {
                    Id = Guid.Parse("8ef5219d-c85e-4ba0-b60f-174e5dfc4a53"),
                    QuizId = Guid.Parse("19a06750-5441-443c-a295-afd16b6a49cf"),
                    QuestionText = "Question test 1",
                    Points = 10,
                    Options = new List<Option>()
                    {
                        new Option()
                        {
                            Id= Guid.Parse("f4d241f2-052f-4849-8d01-6afb29401e21"),
                            QuestionId = Guid.Parse("8ef5219d-c85e-4ba0-b60f-174e5dfc4a53"),
                            Answer = "Answer to Question test 1"
                        }
                    }
                }
            };

            return questionList;
        }
    }
}
