﻿using Microsoft.EntityFrameworkCore;
using QuizApplcation.Infrastructure.Contexts;

namespace QuizApplication.Test.Helpers
{
    public static class DBContextHelper
    {
        public static DbContextOptions BuildDBContextOptions(string databaseName)
        {
            var options = new DbContextOptionsBuilder<DatabaseService>()
                    .UseInMemoryDatabase(databaseName: databaseName)
                    .Options;

            return options;
        }
    }
}
