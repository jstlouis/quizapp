﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Moq;
using Moq.AutoMock;
using QuizApplcation.Application.Interfaces;
using QuizApplcation.Application.Results.Commands.AddResult;
using QuizApplcation.Application.Results.Commands.AddResult.Factory;
using QuizApplcation.Application.Results.Queries;
using QuizApplcation.Domain.Entities;
using QuizApplcation.Infrastructure.Contexts;
using System;
using System.Collections.Generic;
using System.Text;

namespace QuizApplication.Test.Helpers
{
    public static class ResultsCommandHandlerHelper
    {
        public static GetResultQueryHandler MockGetResultQueryHandler()
        {
            AutoMocker mocker = new AutoMocker();

            var entity = MockResultEntity();

            // mock database service with in memory context
            var databaseService = new DatabaseService(DBContextHelper.BuildDBContextOptions("ResultQueryContext"));
            // add the mock entity to in memory context
            databaseService.Add(entity);
            databaseService.SaveChanges();

            mocker.Use<IDatabaseService>(databaseService);

            // mock the query handler
            var mockGetResultQueryHandler = mocker.CreateInstance<GetResultQueryHandler>();

            return mockGetResultQueryHandler;
        }

        public static AddResultCommandHandler MockAddResultCommandHandler()
        {
            AutoMocker mocker = new AutoMocker();

            // mock database service with in memory context
            var databaseService = new DatabaseService(DBContextHelper.BuildDBContextOptions("ResultAddContext"));

            mocker.Use<IDatabaseService>(databaseService);
            mocker.Use<IAddResultFactory>(new AddResultFactory());

            // Mock serviceProvider
            // Based on: https://stackoverflow.com/questions/44336489/moq-iserviceprovider-iservicescope
            var serviceProvider = new Mock<IServiceProvider>();
            serviceProvider
                .Setup(x => x.GetService(typeof(IDatabaseService)))
                .Returns(databaseService);

            // mock IServiceScope to return the above service provider with dbcontext
            var serviceScope = new Mock<IServiceScope>();
            serviceScope.Setup(x => x.ServiceProvider).Returns(serviceProvider.Object);

            // mock the IServiceScopeFactory to return the about service scope
            mocker.GetMock<IServiceScopeFactory>()
                  .Setup(p => p.CreateScope())
                  .Returns(serviceScope.Object);

            // mock the handler
            var mockAddResultCommandHandler = mocker.CreateInstance<AddResultCommandHandler>();

            return mockAddResultCommandHandler;
        }

        public static ResultEntity MockResultEntity()
        {
            var resultEntity = new ResultEntity(Guid.Parse("3e1422c2-3b72-44b0-aa72-e20d2dd020ef"))
            {
                UserId = Guid.Parse("14eabb21-176e-45ab-a570-a0a73bdabb89"),
                QuizId = Guid.Parse("4df81d9c-eae1-4b55-9814-b241dab4ad59"),
                QuestionId = Guid.Parse("dc10e716-ac4b-4442-a7d3-c2f78da1bb17"),
                Score = 100
            };

            return resultEntity;
        }
    }
}
