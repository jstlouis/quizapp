﻿using AutoMapper;
using MediatR;
using Moq;
using Moq.AutoMock;
using QuizApplcation.Application.Quizzes.Queries;
using QuizApplcation.Domain.Entities;
using QuizApplication.RazorPages.Profiles;
using QuizApplication.RazorPages.Quizzes.Factories;
using QuizApplication.RazorPages.Quizzes.Models;
using System;
using System.Collections.Generic;
using System.Threading;

namespace QuizApplication.Test.Helpers
{
    public static class QuizModelFactoryTestHelper
    {
        public static IQuizViewModelFactory MockQuizViewModelFactory()
        {
            AutoMocker mocker = new AutoMocker();

            // mock IMapper
            var quizProfile = new QuizProfile();
            var configuration = new MapperConfiguration(cfg => cfg.AddProfile(quizProfile));
            var mapper = new Mapper(configuration);
            mocker.Use<IMapper>(mapper);

            // mock so quick entities
            var quizEntity = new List<QuizEntity>()
            {
                new QuizEntity(){
                    Id = Guid.Parse("b5b5f4c4-34ed-4770-ae23-9235f377b650"),
                    Title = "Test Quiz 1",
                    Status = 0,
                    State = new QuizStateEntity()
                    {
                        Id = 0,
                        Description = "Not Started"
                    }
                },

                new QuizEntity(){
                    Id = Guid.Parse("664dcfc3-fbff-4730-a37a-d08522cd11a0"),
                    Title = "Test Quiz 2",
                    Status = 1,
                    State = new QuizStateEntity()
                    {
                        Id = 1,
                        Description = "Inprogress"
                    }
                }
            };

            // mock mediator to return the test quiz entities
            mocker.GetMock<IMediator>()
                  .Setup(p => p.Send(It.IsAny<GetQuizzesCommand>(), It.IsAny<CancellationToken>()))
                  .ReturnsAsync(quizEntity);
            
            // mock the factory.
            var mockQuizViewModelFactory = mocker.CreateInstance<QuizViewModelFactory>();

            return mockQuizViewModelFactory;
        }

        public static List<Quiz> MockQuizModel()
        {
            var model = new List<Quiz>()
            {
                new Quiz(){
                    Id = Guid.Parse("b5b5f4c4-34ed-4770-ae23-9235f377b650"),
                    Title = "Test Quiz 1",
                    Status = 0,
                    StatusDescription = "Not Started"                    
                },

                new Quiz(){
                    Id = Guid.Parse("664dcfc3-fbff-4730-a37a-d08522cd11a0"),
                    Title = "Test Quiz 2",
                    Status = 1,
                    StatusDescription = "Inprogress"
                }
            };

            return model;
        }
    }
}
