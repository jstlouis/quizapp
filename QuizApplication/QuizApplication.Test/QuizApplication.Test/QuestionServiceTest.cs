﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using QuizApplcation.Application.Questions.Specifications;
using QuizApplication.RazorPages.Questions.Models;
using QuizApplication.RazorPages.Questions.Services;
using QuizApplication.Test.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuizApplication.Test
{

    [TestClass]
    public class QuestionServiceTest
    {
        private readonly IQuestionService _mockService;

        public QuestionServiceTest()
        {
            _mockService = QuestionServiceHelper.GetMockQuestionService();
        }

        [TestMethod]
        public async Task ShoudReturnZeroScoreWithIncorrectSelection()
        {
            var optionEntity = QuestionServiceHelper.GetMockQuestionEntityList()
                                              .Where(p => p.Id == Guid.Parse("3600a143-ca5b-4bd4-96af-982f221d64ed"))
                                              .FirstOrDefault()
                                              .Options
                                              .FirstOrDefault();

            var optionModel = new List<Option>()
            {
                new Option(){
                    Id = optionEntity.Id,
                    QuestionId = optionEntity.QuestionId,
                    Answer = optionEntity.Answer,
                    Selected = true
                }
            };

            var score = await _mockService.CalculateAndStoreResults(
                Guid.Parse("3600a143-ca5b-4bd4-96af-982f221d64ed"), optionModel);

            Assert.AreEqual(0, score);
        }

        [TestMethod]
        public async Task ShoudReturnZeroScoreWithNoSelection()
        {
            var optionEntity = QuestionServiceHelper.GetMockQuestionEntityList()
                                              .Where(p => p.Id == Guid.Parse("3600a143-ca5b-4bd4-96af-982f221d64ed"))
                                              .FirstOrDefault()
                                              .Options
                                              .FirstOrDefault();

            var optionModel = new List<Option>()
            {
                new Option(){
                    Id = optionEntity.Id,
                    QuestionId = optionEntity.QuestionId,
                    Answer = optionEntity.Answer,
                    Selected = false
                }
            };

            var score = await _mockService.CalculateAndStoreResults(
                Guid.Parse("3600a143-ca5b-4bd4-96af-982f221d64ed"), optionModel);

            Assert.AreEqual(0, score);
        }

        [TestMethod]
        public async Task ShoudReturnNonZeroScoreWithCorrectSelection()
        {
            var optionEntity = QuestionServiceHelper.GetMockQuestionEntityList()
                                              .Where(p => p.Id == Guid.Parse("3600a143-ca5b-4bd4-96af-982f221d64ed"))
                                              .FirstOrDefault()
                                              .Options.Where(p => p.Id== Guid.Parse("f26f5ada-6e10-4025-bee3-44a6f942172b"))
                                              .FirstOrDefault();

            var optionModel = new List<Option>()
            {
                new Option(){
                    Id = optionEntity.Id,
                    QuestionId = optionEntity.QuestionId,
                    Answer = optionEntity.Answer,
                    Selected = optionEntity.AnswerKey.IsCorrect
                }
            };

            var score = await _mockService.CalculateAndStoreResults(
                Guid.Parse("3600a143-ca5b-4bd4-96af-982f221d64ed"), optionModel);

            Assert.AreEqual(20, score);
        }
    }
}
