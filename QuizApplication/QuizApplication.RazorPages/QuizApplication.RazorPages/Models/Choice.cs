﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuizApplication.RazorPages.Models
{
    public class Choice
    {
        public int OptionId { get; set; }
        public bool Selected { get; set; }
    }
}
