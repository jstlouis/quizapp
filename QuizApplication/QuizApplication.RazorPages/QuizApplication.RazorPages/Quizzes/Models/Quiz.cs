﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuizApplication.RazorPages.Quizzes.Models
{
    public class Quiz
    {
        public Guid Id {get; set;}

        public string Title { get; set; }

        public int Status { get; set; }

        public string StatusDescription { get; set; }
    }
}
