﻿using QuizApplication.RazorPages.Quizzes.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace QuizApplication.RazorPages.Quizzes.Factories
{
    public interface IQuizViewModelFactory
    {
        Task<List<Quiz>> GetQuizzes();
    }
}
