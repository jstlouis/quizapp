﻿using AutoMapper;
using MediatR;
using QuizApplcation.Application.Common;
using QuizApplcation.Application.Quizzes.Queries;
using QuizApplcation.Domain.Entities;
using QuizApplication.RazorPages.Quizzes.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace QuizApplication.RazorPages.Quizzes.Factories
{
    public class QuizViewModelFactory : IQuizViewModelFactory
    {
        private readonly IMediator _mediator;
        private readonly IMapper _mapper;

        public QuizViewModelFactory(IMediator mediator,
                                    IMapper mapper)
        {
            _mediator = mediator;
            _mapper = mapper;
        }

        public async Task<List<Quiz>> GetQuizzes()
        {
            var spec = BaseSpecification<QuizEntity>.All;

            var entities = await _mediator.Send(new GetQuizzesCommand
            {
                Specifications = spec
            });

            var result = _mapper.Map<List<QuizEntity>, List<Quiz>>(entities);

            return result;
        }
    }
}
