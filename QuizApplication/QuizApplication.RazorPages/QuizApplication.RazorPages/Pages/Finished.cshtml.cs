using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;
using QuizApplication.RazorPages.Results.Factories;
using QuizApplication.RazorPages.Results.Models;

namespace QuizApplication.RazorPages.Pages
{
    public class FinishedModel : PageModel
    {
        private readonly ILogger<FinishedModel> _logger;
        private readonly IResultsViewModelFactory _factory;

        public FinishedModel(ILogger<FinishedModel> logger, IResultsViewModelFactory factory)
        {
            _logger = logger;
            _factory = factory;
        }

        public Score result { get; private set; }

        public async Task OnGetAsync(Guid quizId)
        {
            var model = await _factory.GetScore(quizId);

            result = model;
        }
    } 
    
}
