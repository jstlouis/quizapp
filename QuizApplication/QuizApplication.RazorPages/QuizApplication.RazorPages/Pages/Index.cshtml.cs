﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;
using QuizApplication.RazorPages.Quizzes.Factories;
using QuizApplication.RazorPages.Quizzes.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuizApplication.RazorPages.Pages
{
    public class IndexModel : PageModel
    {
        private readonly ILogger<IndexModel> _logger;
        private readonly IQuizViewModelFactory _factory;

        public List<Quiz> Items { get; private set; }

        public IndexModel(ILogger<IndexModel> logger, IQuizViewModelFactory factory)
        {
            _logger = logger;
            _factory = factory;
        }

        public async Task OnGetAsync()
        {
            Items = await _factory.GetQuizzes();
        }
    }
}
