using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;
using QuizApplication.RazorPages.Extensions;
using QuizApplication.RazorPages.Questions.Factories;
using QuizApplication.RazorPages.Questions.Models;
using QuizApplication.RazorPages.Questions.Services;

namespace QuizApplication.RazorPages.Pages
{
    public class QuizModel : PageModel
    {
        private readonly ILogger<QuizModel> _logger;
        private readonly IQuestionViewModelFactory _factory;
        private readonly IQuestionService _service;
        const int PLAY_STATE_NOT_STARTED = 0;
        const int PLAY_STATE_INPROCESS = 1;
        const int PLAY_STATE_FINISHED = 2;

        [BindProperty]
        public Question Question { get; private set; }

        [BindProperty]
        public List<Option> Options { get; private set; }

        [BindProperty]
        public int QuestionIndex { get; private set; }

        public QuizModel(ILogger<QuizModel> logger, IQuestionViewModelFactory factory, IQuestionService service)
        {
            _logger = logger;
            _factory = factory;
            _service = service;
        }

        public async Task OnGetAsync(Guid quizId, int quizState, int questionIndex = 0)
        {
            if (quizState == PLAY_STATE_FINISHED)
            {
                await _service.ResetPlayState(quizId);
            }
            else if (quizState == PLAY_STATE_NOT_STARTED) 
            {
                await _service.UpdateQuizStatus(quizId, PLAY_STATE_INPROCESS);
            }

            var questions = await _factory.GetQuestions(quizId);

            HttpContext.Session.SetComplexData("Questions", questions);

            Question = questions.Skip(questionIndex).Take(1).Single();
            
            Options = Question.Options;

            QuestionIndex = questionIndex;
        }

        public async Task<IActionResult> OnPostAsync(int questionIndex, Question question, List<Option> options)
        {
            if (ModelState.IsValid)
            {
                await _service.CalculateAndStoreResults(question.Id, options);
                
                await _service.SavePlayState(question.QuizId, question.Id);
                
                var questions = HttpContext.Session.GetComplexData<List<Question>>("Questions");

                if (questionIndex < questions.Count() - 1)
                {
                    questionIndex += 1;

                    Question = questions.Skip(questionIndex).Take(1).Single();
                    
                    Options = Question.Options;
                    
                    QuestionIndex = questionIndex;

                    ModelState.Clear();

                    return Page();
                }
                else
                {
                    await _service.UpdateQuizStatus(question.QuizId, PLAY_STATE_FINISHED);
                   
                    return RedirectToPage("Finished", new { quizId = question.QuizId });
                }
            }
            else
            {
                // return to page with same question, something went worng.
                Question = question;

                Options = options;

                QuestionIndex = questionIndex;

                return Page();
            }
        }
    }
}
