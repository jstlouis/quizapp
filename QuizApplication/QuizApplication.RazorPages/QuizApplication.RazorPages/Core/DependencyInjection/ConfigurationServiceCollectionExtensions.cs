﻿using Microsoft.Extensions.DependencyInjection;
using QuizApplcation.Application.PlayState.Command.AddPlayState.Factory;
using QuizApplcation.Application.Quizzes.Commands.GetQuizzes.Factory;
using QuizApplcation.Application.Quizzes.Commands.UpdateQuiz.Factory;
using QuizApplcation.Application.Results.Commands.AddResult.Factory;
using QuizApplication.Fakes.Interfaces;
using QuizApplication.Fakes.Users;
using QuizApplication.RazorPages.Questions.Factories;
using QuizApplication.RazorPages.Questions.Services;
using QuizApplication.RazorPages.Quizzes.Factories;
using QuizApplication.RazorPages.Results.Factories;

namespace QuizApplication.RazorPages.Core.DependencyInjection
{
    public static class ConfigurationServiceCollectionExtensions
    {
        public static IServiceCollection AddAppConfiguration(this IServiceCollection services)
        {
            // pages
            services.AddTransient<IQuizViewModelFactory, QuizViewModelFactory>();
            services.AddTransient<IQuestionViewModelFactory, QuestionViewModelFactory>();
            services.AddTransient<IUserInfo, UserInfo>();
            services.AddTransient<IQuestionService, QuestionService>();
            services.AddTransient<IResultsViewModelFactory, ResultsViewModelFactory>();

            // application
            services.AddTransient<IAddResultFactory, AddResultFactory>();
            services.AddTransient<IAddQuizFactory, AddQuizFactory>();
            services.AddTransient<IUpdateQuizFactory, UpdateQuizFactory>();
            services.AddTransient<IPlayStateFactory, PlayStateFactory>();
                        
            return services;
        }
    }
}
