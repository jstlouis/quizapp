﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using QuizApplcation.Application.Interfaces;
using QuizApplcation.Infrastructure.Contexts;

namespace QuizApplication.RazorPages.Core.DependencyInjection
{
    public static class DataServiceCollectionExtensions
    {
        public static IServiceCollection AddDataService(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddDbContext<DatabaseService>(options =>
                        options.UseSqlServer(configuration.GetConnectionString("DefaultConnectionString")));
            
            services.AddTransient<IDatabaseService, DatabaseService>();

            return services;
        }
    }
}
