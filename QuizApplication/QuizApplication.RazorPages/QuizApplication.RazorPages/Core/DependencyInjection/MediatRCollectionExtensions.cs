﻿using Microsoft.Extensions.DependencyInjection;
using MediatR;
using System.Reflection;
using QuizApplcation.Application.Quizzes.Queries;
using QuizApplcation.Domain.Entities;
using System.Collections.Generic;
using QuizApplcation.Application.Questions.Queries;
using QuizApplcation.Application.Results.Queries;
using QuizApplcation.Application.Results.Commands.AddResult;
using System;
using QuizApplcation.Application.Quizzes.Commands.GetQuizzes;
using QuizApplcation.Application.Quizzes.Commands.UpdateQuiz;
using QuizApplcation.Application.PlayState.Command.AddPlayState;
using QuizApplcation.Application.PlayState.Queries;
using QuizApplcation.Application.Results.Commands.RemoveResults;
using QuizApplcation.Application.PlayState.Command.RemovePlayState;

namespace QuizApplication.RazorPages.Core.DependencyInjection
{
    public static class MediatRCollectionExtensions
    {
        public static IServiceCollection AddMediatRService(this IServiceCollection services)
        {
            services.AddMediatR(Assembly.GetExecutingAssembly());
            services.AddTransient<IRequestHandler<GetQuizzesCommand, List<QuizEntity>>, GetQuizzesQueryHandler>();
            services.AddTransient<IRequestHandler<GetQuestionsCommand, List<QuestionEntity>>, GetQuestionsQueryHandler>();
            services.AddTransient<IRequestHandler<GetResultCommand, List<ResultEntity>>, GetResultQueryHandler>();
            services.AddTransient<IRequestHandler<AddResultCommand, Tuple<int, ResultEntity>>, AddResultCommandHandler>();
            services.AddTransient<IRequestHandler<AddQuizCommand, Tuple<int, QuizEntity>>, AddQuizCommandHandler>();
            services.AddTransient<IRequestHandler<UpdateQuizCommand, Tuple<int, QuizEntity>>, UpdateQuizCommandHandler>();
            services.AddTransient<IRequestHandler<AddPlayStateCommand, Tuple<int, PlayStateEntity>>, AddPlayStateCommandHandle>();
            services.AddTransient<IRequestHandler<GetPlayStateCommand, List<PlayStateEntity>>, GetPlayStateCommandHandler>();
            services.AddTransient<IRequestHandler<GetPlayStateCommand, List<PlayStateEntity>>, GetPlayStateCommandHandler>();
            services.AddTransient<IRequestHandler<RemoveResultsCommand, int>, RemoveResultsCommandHandler>();
            services.AddTransient<IRequestHandler<RemovePlayStateCommand, int>, RemovePlayStateCommandHandler>();

            return services;
        }
    }
}
