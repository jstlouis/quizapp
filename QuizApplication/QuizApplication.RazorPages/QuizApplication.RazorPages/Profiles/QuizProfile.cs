﻿using AutoMapper;
using QuizApplcation.Domain.Entities;
using QuizApplication.RazorPages.Quizzes.Models;

namespace QuizApplication.RazorPages.Profiles
{
    public class QuizProfile : Profile
    {
        public QuizProfile()
        {
            CreateMap<QuizEntity, Quiz>()
                .ForMember(dto => dto.Id, opt => opt.MapFrom(src => src.Id))
                .ForMember(dto => dto.Title, opt => opt.MapFrom(src => src.Title))
                .ForMember(dto => dto.Status, opt => opt.MapFrom(src => src.Status))
                .ForMember(dto => dto.StatusDescription, opt => opt.MapFrom(src => src.State.Description));                
        }
    }
}
