﻿using AutoMapper;
using QuizApplcation.Domain.Entities;
using QuizApplication.RazorPages.Questions.Models;

namespace QuizApplication.RazorPages.Profiles
{
    public class QuestionProfile : Profile
    {
        public QuestionProfile()
        {
            CreateMap<QuestionEntity, Question>()
                .ForMember(dto => dto.Id, opt => opt.MapFrom(src => src.Id))
                .ForMember(dto => dto.QuizId, opt => opt.MapFrom(src => src.QuizId))
                .ForMember(dto => dto.QuestionText, opt => opt.MapFrom(src => src.Question))
                .ForMember(dto => dto.Points, opt => opt.MapFrom(src => src.Points))
                .ForMember(dto => dto.Options, opt => opt.MapFrom(src => src.Options));
            CreateMap<OptionsEntity, Option>();
        }
        
    }
}
