﻿using QuizApplication.RazorPages.Questions.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuizApplication.RazorPages.Questions.Services
{
    public interface IQuestionService
    {
        Task<int> CalculateAndStoreResults(Guid questionId, List<Option> choices);
        Task<int> UpdateQuizStatus(Guid quizId, int status);
        Task<int> SavePlayState(Guid quizId, Guid questionId);
        Task<int> ResetPlayState(Guid quizId);
    }
}
