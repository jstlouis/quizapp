﻿using MediatR;
using QuizApplcation.Application.Models;
using QuizApplcation.Application.PlayState.Command.AddPlayState;
using QuizApplcation.Application.PlayState.Command.RemovePlayState;
using QuizApplcation.Application.Questions.Queries;
using QuizApplcation.Application.Questions.Specifications;
using QuizApplcation.Application.Quizzes.Commands.UpdateQuiz;
using QuizApplcation.Application.Results.Commands.AddResult;
using QuizApplcation.Application.Results.Commands.RemoveResults;
using QuizApplication.Fakes.Interfaces;
using QuizApplication.RazorPages.Questions.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuizApplication.RazorPages.Questions.Services
{
    public class QuestionService : IQuestionService
    {
        private readonly IMediator _mediator;
        private readonly IUserInfo _userInfo;
        const int PLAY_STATE_INPROCESS = 1;

        public QuestionService(IMediator mediator, IUserInfo userInfo)
        {
            _mediator = mediator;
            _userInfo = userInfo;
        }

        public async Task<int> CalculateAndStoreResults(Guid questionId, List<Option> choices)
        {
            try { 
                
                var spec = new QuestionIdIsSpecification(questionId);

                var questionList = await _mediator.Send(new GetQuestionsCommand
                {
                    Specifications = spec
                });

                var question = questionList.FirstOrDefault();

                int score = 0;

                if (choices.Any(p => p.Selected))
                {
                    score = question.Points;
                }                

                // if any question is selected that is incorrect zero points given
                foreach (var choice in choices)
                {
                    var option = question.Options.Where(p => p.Id == choice.Id).FirstOrDefault();

                    if (!option.AnswerKey.IsCorrect && choice.Selected)
                    {
                        score = 0;

                        break;
                    }
                }

                var model = new AddResultModel()
                {
                    UserId = _userInfo.UserId,
                    QuizId = question.QuizId,
                    QuestionId = question.Id,
                    Score = score
                };

                var result = await _mediator.Send(new AddResultCommand
                {
                    Model = model
                });

                return score;

            }
            catch(Exception e)
            {
                throw e;
            }
        }

        public async Task<int> ResetPlayState(Guid quizId)
        {
            var result = await _mediator.Send(new RemovePlayStateCommand
            {
                QuizId = quizId
            });

            result += await _mediator.Send(new RemoveResultsCommand
            {
                QuizId = quizId
            });

            result += await UpdateQuizStatus(quizId, PLAY_STATE_INPROCESS);

            return result;
        }

        public async Task<int> SavePlayState(Guid quizId, Guid questionId)
        {
            try
            {
                var model = new AddPlayStateModel()
                {
                    UserId = _userInfo.UserId,
                    QuizzesId = quizId,
                    QuestionId = questionId,
                    IsComplete = true
                };

                var result = await _mediator.Send(new AddPlayStateCommand
                {
                    Model = model
                });

                return result.Item1;

            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public async Task<int> UpdateQuizStatus(Guid quizId, int status)
        {
            try
            {
                var model = new UpdateQuizModel()
                {
                    Id = quizId,
                    Status = status
                };

                var result = await _mediator.Send(new UpdateQuizCommand
                {
                    Model = model
                });

                return result.Item1;

            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}
