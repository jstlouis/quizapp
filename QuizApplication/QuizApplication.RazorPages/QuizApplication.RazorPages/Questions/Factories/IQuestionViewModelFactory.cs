﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using QuizApplication.RazorPages.Questions.Models;

namespace QuizApplication.RazorPages.Questions.Factories
{
    public interface IQuestionViewModelFactory
    {
        Task<List<Question>> GetQuestions(Guid quizId);
    }
}
