﻿using AutoMapper;
using MediatR;
using QuizApplcation.Application.PlayState.Queries;
using QuizApplcation.Application.PlayState.Specifications;
using QuizApplcation.Application.Questions.Queries;
using QuizApplcation.Application.Questions.Specifications;
using QuizApplcation.Domain.Entities;
using QuizApplication.RazorPages.Questions.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuizApplication.RazorPages.Questions.Factories
{
    public class QuestionViewModelFactory : IQuestionViewModelFactory
    {
        private readonly IMediator _mediator;
        private readonly IMapper _mapper;

        public QuestionViewModelFactory(IMediator mediator,
                                        IMapper mapper)
        {
            _mediator = mediator;
            _mapper = mapper;
        }

        public async Task<List<Question>> GetQuestions(Guid quizId)
        {
            var spec = new QuestionQuizIdIsSpecification(quizId);

            var entities = await _mediator.Send(new GetQuestionsCommand
            {
                Specifications = spec
            });

            entities = await FilterByPlayState(quizId, entities);

            Random rnd = new Random();

            // Randomize the questions
            var randomize = entities.OrderBy((item) => rnd.Next());

            // Randomize the options... TODO: This is not randomizing options
            randomize.SelectMany(o => o.Options).ToList().OrderBy((item) => rnd.Next());

            var result = _mapper.Map<List<QuestionEntity>, List<Question>>(randomize.ToList());

            return result;
        }

        internal async Task<List<QuestionEntity>> FilterByPlayState(Guid quizId, List<QuestionEntity> questions)
        {
            var spec = new PlayStateQuizIdIsSpecification(quizId);

            var entities = await _mediator.Send(new GetPlayStateCommand
            {
                Specifications = spec
            });

            var filtered = questions
                   .Where(x => !entities.Any(y => y.QuestionId == x.Id)).ToList();

            return filtered;
        }
    }
}
