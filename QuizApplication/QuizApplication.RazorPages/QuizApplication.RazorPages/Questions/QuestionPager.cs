﻿using Microsoft.AspNetCore.Mvc;
using QuizApplication.RazorPages.Questions.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuizApplication.RazorPages.Questions
{
    public class QuestionPager : ViewComponent
    {
        public async Task<IViewComponentResult> InvokeAsync(Question question)
        {
            return await Task.FromResult(View(question));
        }
    }
}
