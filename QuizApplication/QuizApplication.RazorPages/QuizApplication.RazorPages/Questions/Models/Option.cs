﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace QuizApplication.RazorPages.Questions.Models
{
    public class Option
    {
        [BindProperty]
        [Required]
        public Guid Id { get; set; }

        [BindProperty]
        [Required]
        public Guid QuestionId { get; set; }

        [BindProperty]
        [Required]
        public string Answer { get; set; }

        [BindProperty]
        public bool Selected { get; set; }
    }
}
