﻿using Microsoft.AspNetCore.Mvc;
using QuizApplcation.Domain.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace QuizApplication.RazorPages.Questions.Models
{
    public class Question
    {
        [BindProperty]
        [Required]
        public Guid Id { get; set; }

        [BindProperty]
        [Required]
        public Guid QuizId { get; set; }

        [Required]
        public string QuestionText { get; set; }

        [Required]
        public int Points { get; set; }

        public virtual List<Option> Options { get; set; }
    }
}
