﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuizApplication.RazorPages.Results.Models
{
    public class Score
    {
        public int score { get; set; }

        public int maxScore { get; set; }
    }
}
