﻿using AutoMapper;
using MediatR;
using QuizApplcation.Application.Results.Specifications;
using QuizApplcation.Application.Results.Queries;
using QuizApplication.RazorPages.Results.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using QuizApplcation.Application.Questions.Specifications;
using QuizApplcation.Application.Questions.Queries;

namespace QuizApplication.RazorPages.Results.Factories
{
    public class ResultsViewModelFactory : IResultsViewModelFactory
    {
        private readonly IMediator _mediator;

        public ResultsViewModelFactory(IMediator mediator)
        {
            _mediator = mediator;
        }

        public async Task<Score> GetScore(Guid quizId)
        {
            try
            {
                var spec = new QuizIdIsSpecification(quizId);

                var entities = await _mediator.Send(new GetResultCommand
                {
                    Specifications = spec
                });

                var maxScore = await GetMaxScore(quizId);

                var model = new Score()
                {
                    score = entities.Sum(p => p.Score),
                    maxScore = maxScore
                };

                return model;

            } catch (Exception e)
            {
                throw e;
            }
        }

        internal async Task<int> GetMaxScore(Guid quizId)
        {
            try
            {
                var spec = new QuestionQuizIdIsSpecification(quizId);

                var entities = await _mediator.Send(new GetQuestionsCommand
                {
                    Specifications = spec
                });

                return entities.Sum(p => p.Points);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

    }
}

