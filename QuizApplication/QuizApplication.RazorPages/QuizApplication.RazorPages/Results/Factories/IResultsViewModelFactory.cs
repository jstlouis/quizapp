﻿using QuizApplication.RazorPages.Results.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuizApplication.RazorPages.Results.Factories
{
    public interface IResultsViewModelFactory
    {
        Task<Score> GetScore(Guid quizId);
    }
}
