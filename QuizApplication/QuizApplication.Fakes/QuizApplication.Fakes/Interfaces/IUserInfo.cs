﻿using System;
using System.Collections.Generic;
using System.Text;

namespace QuizApplication.Fakes.Interfaces
{
    public interface IUserInfo
    {
        public Guid UserId { get; set; }
    }
}
