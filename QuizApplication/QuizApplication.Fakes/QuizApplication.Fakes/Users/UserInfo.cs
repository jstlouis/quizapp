﻿using QuizApplication.Fakes.Interfaces;
using System;

namespace QuizApplication.Fakes.Users
{
    public class UserInfo : IUserInfo
    {
        public Guid UserId { get; set; } = Guid.Parse("622fb46e-22c4-4fde-b2bd-9daf675b2ba7");
    }
}
