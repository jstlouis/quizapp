﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace QuizApplcation.Domain.SharedKernel
{
    public abstract class BaseEntity : IEquatable<BaseEntity>
    {
        public Guid Id { get; set; }

        protected BaseEntity(Guid Id)
        {

            if (object.Equals(Id, default(Guid)))
            {
                throw new ArgumentException("The ID cannot be the type's default value.", "Record Id");
            }

            this.Id = Id;
        }

        // EF requires an empty constructor
        protected BaseEntity()
        {
        }

        public override bool Equals(object otherObject)
        {

            var entity = otherObject as BaseEntity;

            if (entity != null)
            {
                return this.Equals(entity);
            }

            return base.Equals(otherObject);

        }

        public override int GetHashCode()
        {
            return this.Id.GetHashCode();
        }

        public bool Equals(BaseEntity other)
        {

            if (other == null)
            {
                return false;
            }

            return this.Id.Equals(other.Id);
        }
    }
}

