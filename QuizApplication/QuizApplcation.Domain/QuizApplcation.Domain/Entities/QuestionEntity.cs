﻿using QuizApplcation.Domain.SharedKernel;
using QuizApplcation.Domain.SharedKernel.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace QuizApplcation.Domain.Entities
{
    public class QuestionEntity : BaseEntity, IEntity
    {
        public QuestionEntity(Guid recordId)
          : base(recordId)
        {
        }

        private QuestionEntity()
            : base(Guid.NewGuid()) // required for EF
        {
        }

        [Required]
        public Guid QuizId { get; set; }

        [Required]
        public string Question { get; set; }

        [Required]
        public int Points { get; set; }

        public virtual List<OptionsEntity> Options { get; set; }
    }
}
