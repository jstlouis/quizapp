﻿using QuizApplcation.Domain.SharedKernel;
using QuizApplcation.Domain.SharedKernel.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace QuizApplcation.Domain.Entities
{
    public class QuizEntity : BaseEntity, IEntity
    {
        public QuizEntity(Guid recordId)
          : base(recordId)
        {
        }

        public QuizEntity()
            : base(Guid.NewGuid()) // required for EF
        {
        }

        [Required]
        [MaxLength(255)]
        public string Title { get; set; }
        
        public int Status { get; set; }

        public virtual List<QuestionEntity> Questions { get; set; }

        public virtual QuizStateEntity State { get; set; }

    }
}
