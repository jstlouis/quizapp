﻿using QuizApplcation.Domain.SharedKernel;
using QuizApplcation.Domain.SharedKernel.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace QuizApplcation.Domain.Entities
{
    public class AnswerEntity : BaseEntity, IEntity
    {
        public AnswerEntity(Guid recordId)
          : base(recordId)
        {
        }

        private AnswerEntity()
            : base(Guid.NewGuid()) // required for EF
        {
        }
        
        [Required]
        public Guid OptionId { get; set; }

        public bool IsCorrect { get; set; }
    }
}
