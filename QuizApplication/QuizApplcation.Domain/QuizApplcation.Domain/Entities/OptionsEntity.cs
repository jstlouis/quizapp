﻿using QuizApplcation.Domain.SharedKernel;
using QuizApplcation.Domain.SharedKernel.Interfaces;
using System;
using System.ComponentModel.DataAnnotations;

namespace QuizApplcation.Domain.Entities
{
    public class OptionsEntity : BaseEntity, IEntity
    {
        public OptionsEntity(Guid recordId)
          : base(recordId)
        {
        }

        private OptionsEntity()
            : base(Guid.NewGuid()) // required for EF
        {
        }

        [Required]
        public Guid QuestionId { get; set; }

        [Required]
        public string Answer { get; set; }

        public virtual AnswerEntity AnswerKey { get; set; }
    }
}
