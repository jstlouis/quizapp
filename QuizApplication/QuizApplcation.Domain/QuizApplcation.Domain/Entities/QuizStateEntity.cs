﻿using QuizApplcation.Domain.SharedKernel;
using QuizApplcation.Domain.SharedKernel.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace QuizApplcation.Domain.Entities
{
    public class QuizStateEntity : IEntity
    {
        public int Id { get; set; }

        public string Description { get; set; }
    }
}
