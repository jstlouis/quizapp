﻿using QuizApplcation.Domain.SharedKernel;
using QuizApplcation.Domain.SharedKernel.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace QuizApplcation.Domain.Entities
{
    public class ResultEntity : BaseEntity, IEntity
    {
        public ResultEntity(Guid recordId)
          : base(recordId)
        {
        }

        public ResultEntity()
            : base(Guid.NewGuid()) // required for EF
        {
        }

        [Required]
        public Guid UserId { get; set; }

        [Required]
        public Guid QuizId { get; set; }

        [Required]
        public Guid QuestionId { get; set; }        

        [Required]
        public int Score { get; set; }
    }
}
