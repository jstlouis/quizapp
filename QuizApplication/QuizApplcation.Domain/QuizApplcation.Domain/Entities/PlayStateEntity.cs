﻿using QuizApplcation.Domain.SharedKernel;
using QuizApplcation.Domain.SharedKernel.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace QuizApplcation.Domain.Entities
{
    public class PlayStateEntity : BaseEntity, IEntity
    {
        public PlayStateEntity(Guid recordId)
          : base(recordId)
        {
        }

        public PlayStateEntity()
            : base(Guid.NewGuid()) // required for EF
        {
        }

        [Required]
        public Guid UserId { get; set; }

        [Required]
        public Guid QuizId { get; set; }

        [Required]
        public Guid QuestionId { get; set; }

        [Required]
        public bool IsComplete { get; set; }
    }
}
