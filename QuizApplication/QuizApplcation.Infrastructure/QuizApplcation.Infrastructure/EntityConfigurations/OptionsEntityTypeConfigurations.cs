﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using QuizApplcation.Domain.Entities;

namespace QuizApplcation.Infrastructure.EntityConfigurations
{
    public class OptionsEntityTypeConfigurations : IEntityTypeConfiguration<OptionsEntity>
    {
        public void Configure(EntityTypeBuilder<OptionsEntity> builder)
        {
            builder.ToTable("Options");

            builder.Property(p => p.Id).ValueGeneratedOnAdd();

            builder.HasKey(p => p.Id);

            builder.HasOne(p => p.AnswerKey)
                   .WithOne()
                   .HasPrincipalKey<AnswerEntity>(p => p.OptionId)
                   .HasForeignKey<OptionsEntity>(p => p.Id);
        }
    }
}
