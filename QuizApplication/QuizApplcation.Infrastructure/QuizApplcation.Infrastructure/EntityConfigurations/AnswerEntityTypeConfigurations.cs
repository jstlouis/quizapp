﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using QuizApplcation.Domain.Entities;

namespace QuizApplcation.Infrastructure.EntityConfigurations
{
    public class AnswerEntityTypeConfigurations : IEntityTypeConfiguration<AnswerEntity>
    {
        public void Configure(EntityTypeBuilder<AnswerEntity> builder)
        {
            builder.ToTable("Answers");

            builder.Property(p => p.Id).ValueGeneratedOnAdd();

            builder.HasKey(p => p.Id);
        }
    }
}
