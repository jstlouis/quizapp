﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using QuizApplcation.Domain.Entities;

namespace QuizApplcation.Infrastructure.EntityConfigurations
{
    public class QuizStateEntityTypeConfigurations : IEntityTypeConfiguration<QuizStateEntity>
    {
        public void Configure(EntityTypeBuilder<QuizStateEntity> builder)
        {
            builder.ToTable("QuizStates");
            
            builder.HasKey(p => p.Id);
        }
    }
}
