﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using QuizApplcation.Domain.Entities;

namespace QuizApplcation.Infrastructure.EntityConfigurations
{
    public class QuestionEntityTypeConfigurations : IEntityTypeConfiguration<QuestionEntity>
    {
        public void Configure(EntityTypeBuilder<QuestionEntity> builder)
        {
            builder.ToTable("Questions");

            builder.Property(p => p.Id).ValueGeneratedOnAdd();

            builder.HasKey(p => p.Id);

            builder.Property(p => p.QuizId).HasColumnName("QuizzesId");

            builder.HasMany(p => p.Options)
                   .WithOne()
                   .HasPrincipalKey(p => p.Id)
                   .HasForeignKey(p1 => p1.QuestionId)
                   .OnDelete(DeleteBehavior.Cascade);
        }
    }
}
