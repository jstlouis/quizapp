﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using QuizApplcation.Domain.Entities;

namespace QuizApplcation.Infrastructure.EntityConfigurations
{
    public class QuizEntityTypeConfigurations : IEntityTypeConfiguration<QuizEntity>
    {
        public void Configure(EntityTypeBuilder<QuizEntity> builder)
        {
            builder.ToTable("Quizzes");

            builder.Property(p => p.Id).ValueGeneratedOnAdd();

            builder.HasKey(p => p.Id);

            builder.HasMany(p => p.Questions)
                   .WithOne()
                   .HasPrincipalKey(p => p.Id)
                   .HasForeignKey(p1 => p1.QuizId)
                   .OnDelete(DeleteBehavior.Cascade);

            builder.HasOne(p => p.State)
                   .WithOne()
                   .HasPrincipalKey<QuizStateEntity>(p => p.Id)
                   .HasForeignKey<QuizEntity>(p => p.Status);
        }
    }
}
