﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using QuizApplcation.Domain.Entities;

namespace QuizApplcation.Infrastructure.EntityConfigurations
{
    public class PlayStateEntityTypeConfigurations : IEntityTypeConfiguration<PlayStateEntity>
    {
        public void Configure(EntityTypeBuilder<PlayStateEntity> builder)
        {
            builder.ToTable("PlayState");

            builder.Property(p => p.Id).ValueGeneratedOnAdd();

            builder.HasKey(p => p.Id);

            builder.Property(p => p.QuizId).HasColumnName("QuizzesId");
        }
    }
}
