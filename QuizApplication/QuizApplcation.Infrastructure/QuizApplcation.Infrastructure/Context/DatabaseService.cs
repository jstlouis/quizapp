﻿using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;
using System.Threading;
using QuizApplcation.Domain.Entities;
using QuizApplcation.Application.Interfaces;
using QuizApplcation.Infrastructure.EntityConfigurations;
using QuizApplcation.Domain.SharedKernel;
using Microsoft.EntityFrameworkCore.ChangeTracking;

namespace QuizApplcation.Infrastructure.Contexts
{
    public class DatabaseService : DbContext, IDatabaseService
    {
        public DatabaseService(DbContextOptions options) : base(options)
        {
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {

            optionsBuilder.EnableSensitiveDataLogging();

            optionsBuilder.EnableDetailedErrors();

            base.OnConfiguring(optionsBuilder);

        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.ApplyConfiguration(new QuizEntityTypeConfigurations());

            builder.ApplyConfiguration(new QuestionEntityTypeConfigurations());

            builder.ApplyConfiguration(new OptionsEntityTypeConfigurations());

            builder.ApplyConfiguration(new AnswerEntityTypeConfigurations());

            builder.ApplyConfiguration(new ResultEntityTypeConfigurations());

            builder.ApplyConfiguration(new QuizStateEntityTypeConfigurations());

            builder.ApplyConfiguration(new PlayStateEntityTypeConfigurations());
        }

        public override async Task<int> SaveChangesAsync(CancellationToken cancallationToken = default)
        {
            return await base.SaveChangesAsync(cancallationToken);
        }

        public override int SaveChanges()
        {            
            return base.SaveChanges();
        }

        public bool AutoDetectChangesEnabled
        {
            get
            {
                return base.ChangeTracker.AutoDetectChangesEnabled;
            }
            set
            {
                base.ChangeTracker.AutoDetectChangesEnabled = value;
            }
        }

        public void DetectChanges()
        {
            base.ChangeTracker.DetectChanges();
        }

        public new EntityEntry<T> Entry<T>(T entity) where T : BaseEntity
        {
            return base.Entry<T>(entity);
        }

        public new DbSet<T> Set<T>() where T : BaseEntity
        {
            return base.Set<T>();
        }
        
        public void SetPropertyModified<T>(T entity, string property, bool value) where T : BaseEntity
        {
            base.Entry<T>(entity).Property(property).IsModified = value;
        }

        public EntityState GetState<T>(T entity) where T : BaseEntity
        {
            return base.Entry<T>(entity).State;
        }

        public DbSet<QuizEntity> Quizzes { get; set; }
        public DbSet<QuestionEntity> Questions { get; set; }
        public DbSet<ResultEntity> Results { get; set; }
        public DbSet<PlayStateEntity> PlayState { get; set; }
    }
}
