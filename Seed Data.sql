DELETE FROM Quizzes;
DELETE FROM Questions;
DELETE FROM Options;
DELETE FROM Answers;
DELETE FROM Results;
DELETE FROM QuizStates;
DELETE FROM PlayState;

INSERT INTO QuizStates (Id, Description)
VALUES 
(0,'Not Started'),
(1,'Inprogress'),
(2,'Completed');

INSERT INTO Quizzes (Id, Title)
VALUES 
('55a077eb-b5a6-47c1-b23c-8b42e5fb4e50','Animal Facts'),
('5c9c5f7b-c85c-4fdc-9f96-a34bc7149867','General Facts');

INSERT INTO Questions (Id, QuizzesId, Question, Points)
VALUES 
--Animal Facts
('ae739802-4603-4eeb-9693-b24d98daffef', '55a077eb-b5a6-47c1-b23c-8b42e5fb4e50', 'Which of the following fact''s about turtles are true.', 90),
('85d7c481-b8fb-4c61-8473-806db6ad56a6', '55a077eb-b5a6-47c1-b23c-8b42e5fb4e50', 'Which of the following fact''s about lions are true.', 50),
('251f8922-8428-4299-990e-4c76cc110c96', '55a077eb-b5a6-47c1-b23c-8b42e5fb4e50', 'Which of the following fact''s about zebras are true.', 60),
('9307a045-c469-4b2a-a2a6-c0a2744070a3', '55a077eb-b5a6-47c1-b23c-8b42e5fb4e50', 'Which of the following fact''s about kangaroo are true.', 40),
--General Facts
('dd9c3d07-2e44-489a-a5f6-cc3d9083428e', '5c9c5f7b-c85c-4fdc-9f96-a34bc7149867', 'Which of the following fact''s about McDonald�s are true.', 80),
('94c7694b-1559-447d-a36f-d3725c20ff8b', '5c9c5f7b-c85c-4fdc-9f96-a34bc7149867', 'Which of the following fact''s about oranges are true.', 50),
('14abafdf-3634-40bf-ac7a-671d90ef28bb', '5c9c5f7b-c85c-4fdc-9f96-a34bc7149867', 'Which of the following fact''s about Canada are true.', 60);


INSERT INTO Options (Id, QuestionId, Answer)
VALUES 
--Animal Facts
('dacdadfd-6296-4eb7-8d99-c1ecd0680b34', 'ae739802-4603-4eeb-9693-b24d98daffef', 'Turtle shells are made up of about 60 bones that are covered by plates called scutes.'),
('7f5ff161-b954-475d-83ed-89c9238a1aa5', 'ae739802-4603-4eeb-9693-b24d98daffef', 'Most turtle species are found in southeastern North America and South Asia.'),
('8f7712f9-03fd-49b2-8065-3dcff5f41ad1', 'ae739802-4603-4eeb-9693-b24d98daffef', 'Most turtles eat popcorn, candy and chip''s.'),
('ebcacc61-10f6-4e8b-be1a-09cdc506fcce', 'ae739802-4603-4eeb-9693-b24d98daffef', 'Turtles buy most of their furiture from Ikea.'),

('55ac292e-173b-44e0-8a06-23ef279d1267', '85d7c481-b8fb-4c61-8473-806db6ad56a6', 'Nearly all wild lions live in Africa, but one small population exists elsewhere.'),
('8f0807c6-3eb6-4e53-88ff-700c42777042', '85d7c481-b8fb-4c61-8473-806db6ad56a6', 'Lions can get their water from plants.'),
('4f1eba08-b185-4ae4-9b96-fb56db93f7b3', '85d7c481-b8fb-4c61-8473-806db6ad56a6', 'Most lions are afraid the dark.'),
('026cbd0a-5c4f-44cf-ae3e-0ec10001ceb1', '85d7c481-b8fb-4c61-8473-806db6ad56a6', 'Lions are the second largest of the big cats, after tigers.'),

('8d4515ee-10d9-4ec4-be17-c2eb1a6a31b8', '251f8922-8428-4299-990e-4c76cc110c96', 'Zebras are wild animals that cannot be domesticated.'),
('13b1289d-2fc1-4938-b453-d9e3123fada8', '251f8922-8428-4299-990e-4c76cc110c96', 'Zebras sometimes eat big mac''s to bulk up for winter hibernation.'),
('f9277792-222a-4d7e-a334-eb6c2806bf0a', '251f8922-8428-4299-990e-4c76cc110c96', 'Zebras are social animals and live together in large groups, called herds.'),
('6c072d66-7737-4c3a-8c11-137bdd29b6e7', '251f8922-8428-4299-990e-4c76cc110c96', 'Sometimes zebras turn yellow if they sit in the sun too long.'),

('f6b9dae6-dbfe-4337-9eff-69ccf4a6e489', '9307a045-c469-4b2a-a2a6-c0a2744070a3', 'Some Kangaroos Can Hop 25 Feet.'),
('c15b7968-5aac-4de8-ad44-b5a852d7d4f3', '9307a045-c469-4b2a-a2a6-c0a2744070a3', 'A Kangaroos tail falls off in the winter and regrows in the spring.'),
('b3299579-63db-4277-b11e-2a251f369be0', '9307a045-c469-4b2a-a2a6-c0a2744070a3', 'Kangaroos are herbivores and like to chew on grasses, herbs and shrubs.'),
('ada9058e-2dc2-47d5-81a9-85bdd5d830d8', '9307a045-c469-4b2a-a2a6-c0a2744070a3', 'Kangaroos often do headstand''s while practicing yoga.'),

--General Facts
('5a931b2e-465e-4a35-a099-790844c3b0e2', 'dd9c3d07-2e44-489a-a5f6-cc3d9083428e', 'McDonald''s once made bubblegum-flavored broccoli.'),
('9ae84289-c264-43fd-a545-20d5651d866c', 'dd9c3d07-2e44-489a-a5f6-cc3d9083428e', 'McDonald''s chicken nuggets contain putty.'),
('33a4c886-d6ff-43ff-b7cd-914a52227e21', 'dd9c3d07-2e44-489a-a5f6-cc3d9083428e', 'McDonald�s patties include cow eyeballs for filler.'),
('811842b6-a0ae-4774-b001-3b050d51b44a', 'dd9c3d07-2e44-489a-a5f6-cc3d9083428e', 'A New McDonald''s Opens Every 14.5 Hours.'),

('72e2c428-406a-471e-b8ac-03c63610386c', '94c7694b-1559-447d-a36f-d3725c20ff8b', 'The first oranges weren''t orange.'),
('11d6088e-18f6-4cad-a148-3f0b31950549', '94c7694b-1559-447d-a36f-d3725c20ff8b', 'Oranges are the largest citrus fruit in the world.'),
('640b16db-e7cc-4862-9477-967d70beb9fe', '94c7694b-1559-447d-a36f-d3725c20ff8b', 'Eating lots of oranges can improve your ability to jump higher.'),
('cecd7a52-eef8-4886-b1bb-02734567c78f', '94c7694b-1559-447d-a36f-d3725c20ff8b', 'Because oranges are rich in vitamin C, eating them can help cure a cold.'),

('7116d23c-9f22-46cd-89a9-2968f1cf1316', '14abafdf-3634-40bf-ac7a-671d90ef28bb', 'Canada has the longest coastline in the world.'),
('9d96f740-d3c6-4611-bc50-4cb3896a9d5e', '14abafdf-3634-40bf-ac7a-671d90ef28bb', 'There are moose and bears everywhere.'),
('5934f636-1bbf-46b2-b55f-201975f139bf', '14abafdf-3634-40bf-ac7a-671d90ef28bb', 'Mounties only wear red and always ride horses.'),
('ebc1ec5a-79f3-42bd-b5ab-d6360597f9d4', '14abafdf-3634-40bf-ac7a-671d90ef28bb', 'Six Canadian cities have more than 1 million residents.');

INSERT INTO Answers(Id, OptionId, IsCorrect)
VALUES 
--Animal Facts
('30a4d577-a0d0-4a44-bfea-2c5f74a62f0a', 'dacdadfd-6296-4eb7-8d99-c1ecd0680b34', 1),
('e9a2a6bc-4650-4d3e-a810-a98680080054', '7f5ff161-b954-475d-83ed-89c9238a1aa5', 1),
('37f29246-b025-4492-abbb-94e423dbf052', '8f7712f9-03fd-49b2-8065-3dcff5f41ad1', 0),
('c701c28c-f6f8-41f9-832c-ab6d86e93d2f', 'ebcacc61-10f6-4e8b-be1a-09cdc506fcce', 0),

('b8ebef52-a094-4606-88ac-2af203107c0c', '55ac292e-173b-44e0-8a06-23ef279d1267', 1),
('ac4f94dd-3608-4dbd-b1a7-86635b3770e8', '8f0807c6-3eb6-4e53-88ff-700c42777042', 1),
('a3305b60-a1a8-43d6-bad6-3ad060558efe', '4f1eba08-b185-4ae4-9b96-fb56db93f7b3', 0),
('659b8820-4e59-41af-869c-2deb94300ee3', '026cbd0a-5c4f-44cf-ae3e-0ec10001ceb1', 1),

('71a45985-4c2a-4b15-9119-7d6980a29473', '8d4515ee-10d9-4ec4-be17-c2eb1a6a31b8', 1),
('8de9b32a-d143-419e-8ccc-cbca2a9845ac', '13b1289d-2fc1-4938-b453-d9e3123fada8', 0),
('80ac3e02-6b60-4d7b-ae4a-098459ac5177', 'f9277792-222a-4d7e-a334-eb6c2806bf0a', 1),
('d23b1ef7-7275-497e-bc47-727fd818c387', '6c072d66-7737-4c3a-8c11-137bdd29b6e7', 0),

('4f763cf1-e097-489b-881a-ef913e6b7dcd', 'f6b9dae6-dbfe-4337-9eff-69ccf4a6e489', 1),
('ace041e5-5e59-468e-921d-301f43667d6b', 'c15b7968-5aac-4de8-ad44-b5a852d7d4f3', 0),
('a3c062b7-683e-40cd-bde1-ac4ab07d1143', 'b3299579-63db-4277-b11e-2a251f369be0', 1),
('8f93eb93-bfaa-4bd7-a242-70e4b09fe74d', 'ada9058e-2dc2-47d5-81a9-85bdd5d830d8', 0),

--General Facts
('382ed8af-5456-4e73-8936-7f41e2ec0033', '5a931b2e-465e-4a35-a099-790844c3b0e2', 1),
('4bbd80e5-e677-48e2-97ef-bfc7dcb3ce4d', '9ae84289-c264-43fd-a545-20d5651d866c', 0),
('6a1326eb-7410-4330-8932-91a202a61433', '33a4c886-d6ff-43ff-b7cd-914a52227e21', 0),
('9486a4bb-bdb4-4857-ae22-6686d5d5ba82', '811842b6-a0ae-4774-b001-3b050d51b44a', 1),

('4fd067dc-ba14-4667-bbea-56aa24818192', '72e2c428-406a-471e-b8ac-03c63610386c', 1),
('6974e4df-59c7-4d6f-ae5b-350499e4ff6c', '11d6088e-18f6-4cad-a148-3f0b31950549', 1),
('0e3b86fe-0958-43a3-8c76-beec477ccfeb', '640b16db-e7cc-4862-9477-967d70beb9fe', 0),
('1a58f6d1-a70b-41f5-b98c-c5655091d2c8', 'cecd7a52-eef8-4886-b1bb-02734567c78f', 0),

('75b2757b-0326-4c4a-9f3b-2ae3621a32cd', '7116d23c-9f22-46cd-89a9-2968f1cf1316', 1),
('7de36762-10d6-4b11-9b3a-c30ed575608d', '9d96f740-d3c6-4611-bc50-4cb3896a9d5e', 0),
('df630d8b-1937-4b95-ad60-c23e52ca63ff', '5934f636-1bbf-46b2-b55f-201975f139bf', 0),
('ee9dcdcc-7a66-4c06-9aae-f44fbd50b422', 'ebc1ec5a-79f3-42bd-b5ab-d6360597f9d4', 1);





